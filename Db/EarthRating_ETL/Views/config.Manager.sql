SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [config].[Manager]
AS
SELECT d.DatafeedId
     , d.Name [DatafeedName]
     , d.Active [DatafeedStatus]
	 , p.Name [Product]
	 , dp.Active [IncludeProduct]
     , a.Name [Achievement]
     , da.Active [IncludeAchievement]
     
     
  FROM [EarthRating_ETL].[config].[Datafeed] d
  JOIN config.DatafeedAchievement da ON da.DataFeedId = d.DatafeedId
  JOIN config.Achievement a ON a.AchievementId = da.AchievementId
  JOIN config.DatafeedProduct dp ON dp.DataFeedId = d.DatafeedId AND dp.ProductId = a.ProductId
  JOIN config.Product p ON p.ProductId = dp.ProductId
GO
