SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		TT
-- Create date: 4/9/2017
-- Description:	Calculate EarthRating. Ported over from EC3.Calulator solution.
-- =============================================
CREATE FUNCTION [etl].[Calculate_EarthRating]
(
    @TotalQuestions SMALLINT,
    @TotalYesAnswers SMALLINT,
    @TotalNaAnswers SMALLINT
)
RETURNS TINYINT
AS
BEGIN
    DECLARE @Rating SMALLINT,
            @Base DECIMAL(4, 2);

    SET @Base =
    (
        SELECT (CAST(@TotalYesAnswers AS DECIMAL(4, 2)) / NULLIF((@TotalQuestions - @TotalNaAnswers),0)) * 5
    );

    SET @Rating = CASE
                      WHEN @Base > 4.5 THEN 5
                      WHEN @Base > 3.5 THEN 4
                      WHEN @Base > 2.5 THEN 3
                      WHEN @Base > 1.5 THEN 2
                      ELSE 1
                  END;

    RETURN @Rating;

END;
GO
