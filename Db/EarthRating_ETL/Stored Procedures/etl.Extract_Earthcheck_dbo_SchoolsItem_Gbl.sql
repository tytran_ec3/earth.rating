SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load Earthcheck.dbo.SchoolsItem_Gbl to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_SchoolsItem_Gbl]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRAN;
    BEGIN TRY

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE [earthcheck_dbo].[SchoolsItem_Gbl];
        END;

        INSERT INTO earthcheck_dbo.SchoolsItem_Gbl
            (
                item_id
              , locale_id
              , item_text
              , item_text_is_translated
              , WH_RunGroup
            )
        SELECT [item_id]
             , [locale_id]
             , [item_text]
             , [item_text_is_translated]
             , @RunGroup [WH_RunGroup]
          FROM [Earthcheck].[dbo].[SchoolsItem_Gbl];

    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
