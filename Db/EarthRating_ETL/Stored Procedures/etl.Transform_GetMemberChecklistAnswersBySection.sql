SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		TT
-- Create date: 12/09/2017
-- Description:	Get the total yes, no and NA answer count by member, checklist and section. Used in the Earth Rating calculation.
-- =============================================
CREATE PROCEDURE [etl].[Transform_GetMemberChecklistAnswersBySection]
AS
BEGIN
    SET NOCOUNT ON;

    WITH cte AS(
	  SELECT tmcr.member_id, cs.checklist_id, cs.section_id, sa.question_id, sig.item_text 
	  FROM [etl].[Transform_MemberChecklistRecords] tmcr
	  JOIN [earthcheck_dbo].[ChecklistRecord_Answer] cra on cra.checklist_record_id = tmcr.checklist_record_id
	  JOIN [earthcheck_dbo].[SchoolsAnswer] sa on sa.answer_id = cra.answer_id
	  JOIN [earthcheck_dbo].[SchoolsQuestion] sq on sq.question_id = sa.question_id
	  JOIN [earthcheck_dbo].[Section_Question] squ on squ.question_id = sq.question_id
	  JOIN [earthcheck_dbo].[SchoolsItem_Gbl] sig on sig.item_id = sa.item_id and locale_id = 3081
	  JOIN earthcheck_dbo.Checklist_Section cs ON cs.checklist_id = tmcr.checklist_id AND cs.section_id = squ.section_id
	  WHERE cs.checklist_id = 17 -- Performance checklist
	)

	SELECT DISTINCT member_id, checklist_id, section_id, GETDATE() WH_ExtractGroup
	,(SELECT count(1) FROM cte a WHERE cte.member_id = a.member_id and cte.checklist_id = a.checklist_id and cte.section_id = a.section_id) 'TotalQuestions'
	,(SELECT count(1) FROM cte a WHERE cte.member_id = a.member_id and cte.checklist_id = a.checklist_id and cte.section_id = a.section_id and a.item_text LIKE 'Yes%') 'YesAnswers'
	,(SELECT count(1) FROM cte a WHERE cte.member_id = a.member_id and cte.checklist_id = a.checklist_id and cte.section_id = a.section_id and a.item_text LIKE 'N/A%') 'NaAnswers'
	FROM cte

END;
GO
