SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Get the total yes, no and NA answer count by member and checklist. Used in the Earth Rating calculation.
-- =============================================
CREATE PROCEDURE [etl].[Transform_GetTotalAnswers]
AS
BEGIN
    SET NOCOUNT ON;

    WITH tq
    AS (SELECT tmcr.member_id,
               tmcr.checklist_id,
               q.question_id
        FROM etl.Transform_MemberChecklistRecords tmcr
            INNER JOIN earthcheck_dbo.Checklist_Section cs ON tmcr.checklist_id = cs.checklist_id
                                                              AND tmcr.[version] = cs.[version]
            INNER JOIN earthcheck_dbo.Section s ON s.section_id = cs.section_id
            INNER JOIN earthcheck_dbo.Section_Question sq ON sq.section_id = s.section_id
            INNER JOIN earthcheck_dbo.SchoolsQuestion q ON q.question_id = sq.question_id
        WHERE q.is_deleted = 0
              AND q.is_hidden = 0
       ),
         ta
    AS (SELECT mc.member_id,
               mc.checklist_id,
               sa.question_id,
               sa.answer_id,
               si.item_id,
               si.item_text
        FROM etl.Transform_MemberChecklistRecords mc
            INNER JOIN earthcheck_dbo.ChecklistRecord_Answer cra ON cra.checklist_record_id = mc.checklist_record_id
            INNER JOIN earthcheck_dbo.SchoolsAnswer sa ON sa.answer_id = cra.answer_id
            INNER JOIN earthcheck_dbo.SchoolsItem_Gbl si ON sa.item_id = si.item_id
                                                            AND si.locale_id = 3081
       )
    SELECT DISTINCT
        ta.member_id,
        ta.checklist_id,
        (
            SELECT COUNT(1)
            FROM ta a
            WHERE (
                      a.member_id = ta.member_id
                      AND a.checklist_id = ta.checklist_id
                      AND a.item_text LIKE 'Yes%'
                  )
        ) 'TotalYesAnswers',
        (
            SELECT COUNT(1)
            FROM ta a
            WHERE (
                      a.member_id = ta.member_id
                      AND a.checklist_id = ta.checklist_id
                      AND a.item_text LIKE 'No%'
                  )
        ) 'TotalNoAnswers',
        (
            SELECT COUNT(1)
            FROM ta a
            WHERE (
                      a.member_id = ta.member_id
                      AND a.checklist_id = ta.checklist_id
                      AND a.item_text LIKE 'N/A%'
                  )
        ) 'TotalNaAnswers',
        (
            SELECT COUNT(1) 'TotalQuestions'
            FROM tq
            WHERE tq.member_id = ta.member_id
                  AND tq.checklist_id = ta.checklist_id
            GROUP BY tq.member_id,
                     tq.checklist_id
        ) AS 'TotalQuestions',
		GETDATE() AS WH_ExtractGroup
    FROM ta;

END;
GO
