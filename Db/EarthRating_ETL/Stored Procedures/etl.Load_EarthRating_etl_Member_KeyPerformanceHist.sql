SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ==========================================================================================
-- Type			Date		Author	Description
-- Created:		05/09/2017	Ty		Loading member details to EarthRating.dbo.Member_EarthRating
-- Modified:	26/09/2017	Ty		Updated to a MERGE Statement to allow for updates.
-- ==========================================================================================

CREATE PROCEDURE [etl].[Load_EarthRating_etl_Member_KeyPerformanceHist]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

	DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	, @Message VARCHAR(MAX), @RowCount INT


        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE [EarthRating].[etl].[Member_KeyPerformanceHist];
        END;
		INSERT INTO EarthRating.etl.Member_KeyPerformanceHist
		    (
		        Member_Id
		      , Checklist_Episode_Id
		      , Checklist_Id
		      , GreenhouseGasEmissions
		      , EnergyEfficiencyConservationAndMgt
		      , MgtOfFreshwaterResources
		      , EcosystemConservationAndMgt
		      , SocialAndCulturalMgt
		      , LandUsePlanningAndMgt
		      , AirQualityProtection
		      , WastewaterMgt
		      , SolidWasteMgt
		      , EnviroHarmfulSubstances
		      , WH_RunGroup
		    )
    SELECT  DISTINCT [member_id]
	  ,checklist_episode_id
      ,[checklist_id]
      ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySectionHist] WHERE cr.member_id = member_id AND cr.checklist_episode_id = checklist_episode_id AND SubSection = 1) 'Greenhouse Gas Emissions'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySectionHist] WHERE cr.member_id = member_id AND cr.checklist_episode_id = checklist_episode_id AND SubSection = 2) 'Energy Efficiency, Conservation and Management'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySectionHist] WHERE cr.member_id = member_id AND cr.checklist_episode_id = checklist_episode_id AND SubSection = 3) 'Management of Freshwater Resources'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySectionHist] WHERE cr.member_id = member_id AND cr.checklist_episode_id = checklist_episode_id AND SubSection = 4) 'Ecosystems Conservation and Management'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySectionHist] WHERE cr.member_id = member_id AND cr.checklist_episode_id = checklist_episode_id AND SubSection = 5) 'Management of Social and Cultural Issues'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySectionHist] WHERE cr.member_id = member_id AND cr.checklist_episode_id = checklist_episode_id AND SubSection = 6) 'Land Use Planning and Management'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySectionHist] WHERE cr.member_id = member_id AND cr.checklist_episode_id = checklist_episode_id AND SubSection = 7) 'Air Quality Protection'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySectionHist] WHERE cr.member_id = member_id AND cr.checklist_episode_id = checklist_episode_id AND SubSection = 8) 'Wastewater Management'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySectionHist] WHERE cr.member_id = member_id AND cr.checklist_episode_id = checklist_episode_id AND SubSection = 9) 'Solid Waste Management'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySectionHist] WHERE cr.member_id = member_id AND cr.checklist_episode_id = checklist_episode_id AND SubSection = 10)'Management of Environmentally Harmful Substances'
      , @RunGroup
  FROM [etl].[Transform_MemberChecklistRatingBySectionHist] cr


SET @RowCount = @@ROWCOUNT

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE()

        ROLLBACK TRANSACTION;

		EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                    , @ObjectName = @ObjectName 
							, @Message = @Message

    END CATCH;
    IF ISNULL(@@TRANCOUNT,0) > 0
        COMMIT TRANSACTION;

		SET @Message = 'Completed'
		EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName
							  , @RowCount = @RowCount
							  , @Message = @Message
END;
GO
