SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Get the total question count by member and checklist. Used in the Earth Rating calculation.
-- =============================================
CREATE PROCEDURE [etl].[Transform_GetTotalQuestionsByMemberChecklist]
AS
BEGIN
    SET NOCOUNT ON;

    WITH cte
    AS (SELECT tmcr.member_id,
               tmcr.checklist_id,
               q.question_id
        FROM etl.Transform_MemberChecklistRecords tmcr
            INNER JOIN earthcheck_dbo.Checklist_Section cs ON tmcr.checklist_id = cs.checklist_id
                                                              AND tmcr.[version] = cs.[version]
            INNER JOIN earthcheck_dbo.Section s ON s.section_id = cs.section_id
            INNER JOIN earthcheck_dbo.Section_Question sq ON sq.section_id = s.section_id
            INNER JOIN earthcheck_dbo.SchoolsQuestion q ON q.question_id = sq.question_id
        WHERE q.is_deleted = 0
              AND q.is_hidden = 0
       )
    SELECT cte.member_id,
           cte.checklist_id,
           COUNT(1) 'TotalQuestions'
    FROM cte
    GROUP BY cte.member_id,
             cte.checklist_id;
END;
GO
