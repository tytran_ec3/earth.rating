SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ==========================================================================================
-- Type			Date		Author	Description
-- Created:		05/09/2017	Ty		Loading member details to EarthRating.dbo.Member_EarthRating
-- Modified:	26/09/2017	Ty		Updated to a MERGE Statement to allow for updates.
-- ==========================================================================================

CREATE PROCEDURE [etl].[Load_EarthRating_etl_Member_KeyPerformance]
AS
BEGIN
    SET NOCOUNT ON;

    SELECT  DISTINCT [member_id]
      ,[checklist_id]
      ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySection] WHERE a.member_id = member_id AND SubSection = 1) 'Greenhouse Gas Emissions'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySection] WHERE a.member_id = member_id AND SubSection = 2) 'Energy Efficiency, Conservation and Management'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySection] WHERE a.member_id = member_id AND SubSection = 3) 'Management of Freshwater Resources'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySection] WHERE a.member_id = member_id AND SubSection = 4) 'Ecosystems Conservation and Management'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySection] WHERE a.member_id = member_id AND SubSection = 5) 'Management of Social and Cultural Issues'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySection] WHERE a.member_id = member_id AND SubSection = 6) 'Land Use Planning and Management'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySection] WHERE a.member_id = member_id AND SubSection = 7) 'Air Quality Protection'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySection] WHERE a.member_id = member_id AND SubSection = 8) 'Wastewater Management'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySection] WHERE a.member_id = member_id AND SubSection = 9) 'Solid Waste Management'
	  ,(SELECT EarthRating FROM [etl].[Transform_MemberChecklistRatingBySection] WHERE a.member_id = member_id AND SubSection = 10)'Management of Environmentally Harmful Substances'
  FROM [etl].[Transform_MemberChecklistRatingBySection] a


END;
GO
