SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Calculate the ratings for individual checklists
-- Modified: 2017-10-13 - Add nullif to allow incomplete assessments through.
-- =============================================
CREATE PROCEDURE [etl].[Transform_CalculateChecklistRating]
AS
BEGIN
    SET NOCOUNT ON;

	WITH cte AS (
    SELECT tmca.member_id,
           tmca.checklist_id,
           (NULLIF(CAST(tmca.TotalYesAnswers AS DECIMAL(4, 2)),0) / NULLIF(tmca.TotalQuestions - tmca.TotalNaAnswers,0)) * 5 'Rating',
           ggal.achievement_level_name 'AchievementLevel'
    FROM [etl].[Transform_MemberChecklistAnswers] tmca
        LEFT JOIN ec3global_dbo.GreenGlobeAchievementHistory ggah ON ggah.member_id = tmca.member_id
                                                         AND ggah.achievement_history_id IS NOT NULL
		LEFT JOIN ec3global_dbo.GreenGlobeAchievementLevel ggal ON ggal.achievement_level_id = ggah.achievement_level_id
    WHERE (NULLIF(tmca.TotalYesAnswers, 0) / (tmca.TotalQuestions - tmca.TotalNaAnswers)) IS NOT NULL
          --AND gg.achievement_expiry_date >= GETDATE()
    )
	SELECT cte.member_id,
           cte.checklist_id,
           CASE 
			WHEN cte.Rating > 4.5 THEN 5
			WHEN cte.Rating > 3.5 THEN 4
			WHEN cte.Rating > 2.5 THEN 3
			WHEN cte.Rating > 1.5 THEN 2
			ELSE 1 END AS 'Rating',
           cte.AchievementLevel 
	FROM cte
END;
GO
