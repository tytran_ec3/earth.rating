SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load Earthcheck.dbo.ChecklistRecord to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_ChecklistRecord]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    IF @Truncate = 1
    BEGIN
        TRUNCATE TABLE [earthcheck_dbo].[ChecklistRecord];
    END;

    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

        INSERT INTO earthcheck_dbo.ChecklistRecord
            (
                checklist_record_id
              , checklist_id
              , version
              , member_id
              , add_timestamp
              , submission_timestamp
              , checklist_episode_id
              , WH_RunGroup
            )
        SELECT cr.[checklist_record_id]
             , cr.[checklist_id]
             , cr.[version]
             , cr.[member_id]
             , NULL AS [add_timestamp]
             , NULL AS [submission_timestamp]
             , cr.[checklist_episode_id]
             , @RunGroup AS [WH_RunGroup]
          FROM ec3global_dbo.Member_Product mp
              JOIN Earthcheck.dbo.[ChecklistRecord] cr ON cr.member_id = mp.member_id;

    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
