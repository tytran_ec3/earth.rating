SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		TT
-- Create date: 12/09/2017
-- Description:	Get the total yes, no and NA answer count by member, checklist and section. Used in the Earth Rating calculation.
-- =============================================
CREATE PROCEDURE [etl].[Transform_GetMemberChecklistAnswersBySectionHist]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

	DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	, @Message VARCHAR(MAX), @RowCount INT

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE etl.Transform_MemberChecklistAnswersBySectionHist;
        END;

        WITH cte
        AS (SELECT tmcr.member_id
                 , tmcr.LatestChecklistEpisode
                 , cs.checklist_id
                 , cs.section_id
                 , sa.question_id
                 , sig.item_text
              FROM [etl].[Transform_MemberChecklistRecordsHist] tmcr
                  JOIN [earthcheck_dbo].[ChecklistRecord_AnswerHist] cra ON cra.checklist_record_id = tmcr.checklist_record_id
                  JOIN [earthcheck_dbo].[SchoolsAnswerHist] sa ON sa.answer_id = cra.answer_id
                  JOIN [earthcheck_dbo].[SchoolsQuestion] sq ON sq.question_id = sa.question_id
                  JOIN [earthcheck_dbo].[Section_Question] squ ON squ.question_id = sq.question_id
                  JOIN [earthcheck_dbo].[SchoolsItem_Gbl] sig ON sig.item_id = sa.item_id
                                                                 AND locale_id = 3081
                  JOIN earthcheck_dbo.Checklist_Section cs ON cs.checklist_id = tmcr.checklist_id
                                                              AND cs.section_id = squ.section_id
             WHERE cs.checklist_id = 17 -- Performance checklist
           )
        INSERT INTO etl.Transform_MemberChecklistAnswersBySectionHist
            (
                member_id
              , checklist_episode_id
              , checklist_id
              , section_id
			  , TotalQuestions
              , TotalYesAnswers
              , TotalNaAnswers
              
              , WH_RunGroup
            )
        SELECT DISTINCT
            member_id
          , LatestChecklistEpisode
          , checklist_id
          , section_id
          , (
                SELECT COUNT(1)
                  FROM cte a
                 WHERE cte.member_id = a.member_id
                       AND cte.checklist_id = a.checklist_id
                       AND cte.section_id = a.section_id
            ) 'TotalQuestions'
          , (
                SELECT COUNT(1)
                  FROM cte a
                 WHERE cte.member_id = a.member_id
                       AND cte.checklist_id = a.checklist_id
                       AND cte.section_id = a.section_id
                       AND a.item_text LIKE 'Yes%'
            ) 'YesAnswers'
          , (
                SELECT COUNT(1)
                  FROM cte a
                 WHERE cte.member_id = a.member_id
                       AND cte.checklist_id = a.checklist_id
                       AND cte.section_id = a.section_id
                       AND a.item_text LIKE 'N/A%'
            ) 'NaAnswers'
			 , @RunGroup WH_RunGroup
          FROM cte;

    	SET @RowCount = @@ROWCOUNT

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE()
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

			EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName 
							  , @Message = @Message

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

		SET @Message = 'Completed'
		EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName
							  , @RowCount = @RowCount
							  , @Message = @Message

END;

GO
