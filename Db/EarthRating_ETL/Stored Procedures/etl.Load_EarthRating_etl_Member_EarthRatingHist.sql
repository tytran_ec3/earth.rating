SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==========================================================================================
-- Type			Date		Author	Description
-- Created:		05/09/2017	Ty		Loading member details to EarthRating.dbo.Member_EarthRating
-- Modified:	26/09/2017	Ty		Updated to a MERGE Statement to allow for updates.
-- ==========================================================================================

CREATE PROCEDURE [etl].[Load_EarthRating_etl_Member_EarthRatingHist]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

        DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
              , @Message VARCHAR(MAX)
              , @RowCount INT;

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE [EarthRating].[etl].[Member_EarthRatingHist];
        END;

		INSERT INTO EarthRating.etl.Member_EarthRatingHist
		    (
		        Member_Id
		      , Checklist_Episode_Id
		      , Checklist_Id
		      , ChecklistName
		      , EarthRating
		      , WH_RunGroup
		    )
        SELECT tmcr.member_id
             , tmcr.checklist_episode_id
             , tmcr.checklist_id
             , CAST(SUBSTRING(c.name, CHARINDEX(' ', c.name) + 1, LEN(c.name)) AS NVARCHAR(100)) [ChecklistName]
             , CAST(tmcr.Rating AS TINYINT) 'EarthRating'
			 , @RunGroup
          FROM etl.Transform_MemberChecklistRatingHist tmcr
              JOIN earthcheck_dbo.Checklist c ON c.checklist_id = tmcr.checklist_id;

        SET @RowCount = @@ROWCOUNT;

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE();
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

        EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
                              , @ObjectName = @ObjectName
                              , @Message = @Message;

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

    SET @Message = 'Completed';
    EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
                          , @ObjectName = @ObjectName
                          , @RowCount = @RowCount
                          , @Message = @Message;
END;
GO
