SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Calculate the ratings for individual Perform sections
-- =============================================
CREATE PROCEDURE [etl].[Transform_CalculateChecklistRatingBySection]
AS
BEGIN
    SET NOCOUNT ON;

	DECLARE  @TotalAnswers Table(
	[member_id] [int] NULL,
	[checklist_id] [int] NULL,
	--[section_id] [int] NULL,
	[TotalQuestions] [int] NULL,
	[TotalNaAnswers] [int] NULL,
	[TotalYesAnswers] [int] NULL,
	[SubSection] [int] NULL,
	[Name] [nvarchar](1000) NULL,
	WH_ExtractGroup DATETIME NULL
)
	INSERT INTO @TotalAnswers
	select member_id, 
	checklist_id, 
	--a.section_id 	,
	[TotalQuestions],
	[TotalNaAnswers],
	[TotalYesAnswers], 
	CAST(CASE
		   WHEN CHARINDEX(' ', Name) =  5 THEN
               SUBSTRING(Name, 3, 2)
           WHEN CHARINDEX(' ', Name) <= 6 THEN
               SUBSTRING(Name, 3, 1)
           WHEN CHARINDEX(' ', Name) >= 7 THEN
               SUBSTRING(Name, 3, 2)
       END AS INT) 'SubSection',
	   SUBSTRING(Name,CHARINDEX(' ',Name)+1,LEN(Name)) Name,
	   GETDATE()
		from  [etl].[Transform_MemberChecklistAnswersBySection] a
		join [earthcheck_dbo].[Section] s on  s.section_id = a.section_id
			
		-- End results
		select ta.member_id, 
		ta.checklist_id,
		ta.SubSection, 
		sum(ta.TotalQuestions)TotalQuestions, 
		SUM(ta.TotalNaAnswers)TotalNaAnswers, 
		SUM(ta.TotalYesAnswers)TotalYesAnswers,
		 [etl].[Calculate_EarthRating](sum(ta.TotalQuestions),	SUM(ta.TotalYesAnswers),SUM(ta.TotalNaAnswers)) EarthRating, 
		GETDATE() WH_ExtractGroup
		from  @TotalAnswers ta 
		
		GROUP BY ta.member_id, ta.checklist_id, ta.SubSection
		ORDER BY SubSection

END;
GO
