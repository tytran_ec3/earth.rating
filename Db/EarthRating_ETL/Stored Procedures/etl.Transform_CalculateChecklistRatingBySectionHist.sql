SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Calculate the ratings for individual Perform sections. Load into etl.Transform_MemberChecklistRatingBySection
-- =============================================
CREATE PROCEDURE [etl].[Transform_CalculateChecklistRatingBySectionHist]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

	DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	, @Message VARCHAR(MAX), @RowCount INT

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE etl.Transform_MemberChecklistRatingBySectionHist;
        END;

        DECLARE @TotalAnswers TABLE
            (
                [member_id] [INT] NULL
              , [checklist_episode_id] INT NULL
              , [checklist_id] [INT] NULL
              , [section_id] [INT] NULL
              , [TotalQuestions] [INT] NULL
              , [TotalNaAnswers] [INT] NULL
              , [TotalYesAnswers] [INT] NULL
              , [SubSection] [INT] NULL
              , [Name] [NVARCHAR](1000) NULL
              , WH_RunGroup INT NULL
            );
        INSERT INTO @TotalAnswers
        SELECT member_id
             , checklist_episode_id
             , checklist_id
             , a.section_id
             , [TotalQuestions]
             , [TotalNaAnswers]
             , [TotalYesAnswers]
             , CAST(CASE
                        WHEN CHARINDEX(' ', name) = 5 THEN
                            SUBSTRING(name, 3, 2)
                        WHEN CHARINDEX(' ', name) <= 6 THEN
                            SUBSTRING(name, 3, 1)
                        WHEN CHARINDEX(' ', name) >= 7 THEN
                            SUBSTRING(name, 3, 2)
                    END AS INT) 'SubSection'
             , SUBSTRING(name, CHARINDEX(' ', name) + 1, LEN(name)) Name
             , @RunGroup
          FROM [etl].[Transform_MemberChecklistAnswersBySectionHist] a
              JOIN [earthcheck_dbo].[Section] s ON s.section_id = a.section_id;

        -- End results
        INSERT INTO etl.Transform_MemberChecklistRatingBySectionHist
            (
                member_id
              , checklist_episode_id
              , checklist_id
			  , SubSection
              , TotalQuestions
              , TotalNaAnswers
              , TotalYesAnswers
              , EarthRating
              , WH_RunGroup
            )
        SELECT ta.member_id
             , ta.checklist_episode_id
             , ta.checklist_id
             , ta.SubSection
             , SUM(ta.TotalQuestions) TotalQuestions
             , SUM(ta.TotalNaAnswers) TotalNaAnswers
             , SUM(ta.TotalYesAnswers) TotalYesAnswers
             , [etl].[Calculate_EarthRating](SUM(ta.TotalQuestions), SUM(ta.TotalYesAnswers), SUM(ta.TotalNaAnswers)) EarthRating
             , @RunGroup WH_RunGroup
          FROM @TotalAnswers ta
         GROUP BY ta.member_id
                , ta.checklist_episode_id
                , ta.checklist_id
                , ta.SubSection
         ORDER BY ta.member_id
                , ta.checklist_episode_id
                , SubSection;

    SET @RowCount = @@ROWCOUNT

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE()
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

			EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName 
							  , @Message = @Message

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

		SET @Message = 'Completed'
		EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName
							  , @RowCount = @RowCount
							  , @Message = @Message
END;

GO
