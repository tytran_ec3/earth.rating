SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load Earthcheck.dbo.Section to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_Section]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRAN;
    BEGIN TRY

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE [earthcheck_dbo].[Section];
        END;

        INSERT INTO earthcheck_dbo.Section
            (
                section_id
              , name
              , text
              , translation_required
              , WH_RunGroup
            )
        SELECT [section_id]
             , [name]
             , [text]
             , [translation_required]
             , @RunGroup [WH_RunGroup]
          FROM [Earthcheck].[dbo].[Section];

    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
