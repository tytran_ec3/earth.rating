SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Get the total yes, no and NA answer count by member and checklist. Used in the Earth Rating calculation.
-- =============================================
CREATE PROCEDURE [etl].[Transform_GetTotalAnswersHist] @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

	DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	, @Message VARCHAR(MAX), @RowCount INT

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE etl.Transform_MemberChecklistAnswersHist
        END;

    WITH tq
    AS (SELECT tmcr.member_id,
				tmcr.LatestChecklistEpisode,
               tmcr.checklist_id,
               q.question_id
        FROM etl.Transform_MemberChecklistRecordsHist tmcr
            INNER JOIN earthcheck_dbo.Checklist_Section cs ON tmcr.checklist_id = cs.checklist_id
                                                              AND tmcr.[version] = cs.[version]
            INNER JOIN earthcheck_dbo.Section s ON s.section_id = cs.section_id
            INNER JOIN earthcheck_dbo.Section_Question sq ON sq.section_id = s.section_id
            INNER JOIN earthcheck_dbo.SchoolsQuestion q ON q.question_id = sq.question_id
        WHERE q.is_deleted = 0
              AND q.is_hidden = 0
       ),
         ta
    AS (SELECT mc.member_id,
				mc.LatestChecklistEpisode,
               mc.checklist_id,
               sa.question_id,
               sa.answer_id,
               si.item_id,
               si.item_text
        FROM etl.Transform_MemberChecklistRecordsHist mc
            INNER JOIN earthcheck_dbo.ChecklistRecord_AnswerHist cra ON cra.checklist_record_id = mc.checklist_record_id
            INNER JOIN earthcheck_dbo.SchoolsAnswerHist sa ON sa.answer_id = cra.answer_id
            INNER JOIN earthcheck_dbo.SchoolsItem_Gbl si ON sa.item_id = si.item_id
                                                            AND si.locale_id = 3081
       )
	   INSERT INTO etl.Transform_MemberChecklistAnswersHist
	       (
	           member_id
	         , checklist_episode_id
	         , checklist_id
	         , TotalYesAnswers
	         , TotalNoAnswers
	         , TotalNaAnswers
	         , TotalQuestions
	         , WH_RunGroup
	       )
    SELECT DISTINCT
        ta.member_id,
		ta.LatestChecklistEpisode,
        ta.checklist_id,
        (
            SELECT COUNT(1)
            FROM ta a
            WHERE (
                      a.member_id = ta.member_id
					  AND a.LatestChecklistEpisode = ta.LatestChecklistEpisode
                      AND a.checklist_id = ta.checklist_id
                      AND a.item_text LIKE 'Yes%'
                  )
        ) 'TotalYesAnswers',
        (
            SELECT COUNT(1)
            FROM ta a
            WHERE (
                      a.member_id = ta.member_id
					  AND a.LatestChecklistEpisode = ta.LatestChecklistEpisode
                      AND a.checklist_id = ta.checklist_id
                      AND a.item_text LIKE 'No%'
                  )
        ) 'TotalNoAnswers',
        (
            SELECT COUNT(1)
            FROM ta a
            WHERE (
                      a.member_id = ta.member_id
					  AND a.LatestChecklistEpisode = ta.LatestChecklistEpisode
                      AND a.checklist_id = ta.checklist_id
                      AND a.item_text LIKE 'N/A%'
                  )
        ) 'TotalNaAnswers',
        (
            SELECT COUNT(1) 'TotalQuestions'
            FROM tq
            WHERE tq.member_id = ta.member_id
				  AND tq.LatestChecklistEpisode = ta.LatestChecklistEpisode
                  AND tq.checklist_id = ta.checklist_id
            GROUP BY tq.member_id,
						tq.LatestChecklistEpisode,
                     tq.checklist_id
        ) AS 'TotalQuestions',
		@RunGroup WH_RunGroup
    FROM ta;

	SET @RowCount = @@ROWCOUNT

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE()
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

			EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName 
							  , @Message = @Message

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

		SET @Message = 'Completed'
		EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName
							  , @RowCount = @RowCount
							  , @Message = @Message
END;
GO
