SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Load EC3Global_dbo.GreenGlobeStatus to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_EC3Global_dbo_GreenGlobeStatus] 
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

	SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

	DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	, @Message VARCHAR(MAX), @RowCount INT

    SET NOCOUNT ON;
    IF @Truncate = 1
    BEGIN
        TRUNCATE TABLE [ec3global_dbo].[GreenGlobeStatus];
    END;

        INSERT INTO ec3global_dbo.GreenGlobeStatus
            (
                status_id
              , status_name
              , WH_RunGroup
            )
        SELECT [status_id]
             , [status_name]
             , @RunGroup
          FROM [EC3Global].[dbo].[GreenGlobeStatus];

    SET @RowCount = @@ROWCOUNT

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE()
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

			EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName 
							  , @Message = @Message

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

		SET @Message = 'Completed'
		EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName
							  , @RowCount = @RowCount
							  , @Message = @Message
		
END;
GO
