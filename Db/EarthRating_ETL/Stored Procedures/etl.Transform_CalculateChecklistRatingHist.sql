SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Calculate the ratings for individual checklists
-- Modified: 2017-10-13 - Add nullif to allow incomplete assessments through.
-- =============================================
CREATE PROCEDURE [etl].[Transform_CalculateChecklistRatingHist]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

	DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	, @Message VARCHAR(MAX), @RowCount INT

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE etl.Transform_MemberChecklistRatingHist;
        END;

        WITH cte
        AS (SELECT tmca.member_id
                 , tmca.checklist_episode_id
                 , tmca.checklist_id
                 , (NULLIF(CAST(tmca.TotalYesAnswers AS DECIMAL(4, 2)), 0)
                    / (NULLIF(tmca.TotalQuestions - tmca.TotalNaAnswers, 0))
                   ) * 5 'Rating'
                 , ggal.achievement_level_name 'AchievementLevel'
              FROM [etl].[Transform_MemberChecklistAnswersHist] tmca
                 LEFT JOIN ec3global_dbo.GreenGlobeProfile ggp ON ggp.member_id = tmca.member_id
                 LEFT JOIN ec3global_dbo.GreenGlobeAchievementHistory ggah ON ggah.achievement_history_id = ggp.achievement_history_id
		LEFT JOIN ec3global_dbo.GreenGlobeAchievementLevel ggal ON ggal.achievement_level_id = ggah.achievement_level_id
             WHERE (NULLIF(tmca.TotalYesAnswers, 0) / (tmca.TotalQuestions - tmca.TotalNaAnswers)) IS NOT NULL
           --AND gg.achievement_expiry_date >= GETDATE()
           )
        INSERT INTO etl.Transform_MemberChecklistRatingHist
            (
                member_id
              , checklist_episode_id
              , checklist_id
              , Rating
              , AchievementLevel
            )
        SELECT cte.member_id
             , cte.checklist_episode_id
             , cte.checklist_id
             , CASE
                   WHEN cte.Rating > 4.5 THEN
                       5
                   WHEN cte.Rating > 3.5 THEN
                       4
                   WHEN cte.Rating > 2.5 THEN
                       3
                   WHEN cte.Rating > 1.5 THEN
                       2
                   ELSE
                       1
               END AS 'Rating'
             , cte.AchievementLevel
          FROM cte;
    
	SET @RowCount = @@ROWCOUNT

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE()
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

			EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName 
							  , @Message = @Message

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

		SET @Message = 'Completed'
		EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName
							  , @RowCount = @RowCount
							  , @Message = @Message

END;

GO
