SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load Earthcheck.dbo.ChecklistRecord_Answer to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_ChecklistRecord_AnswerHist]
    @RunGroup INT = NULL
  , @Truncate INT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

	DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	, @Message VARCHAR(MAX), @RowCount INT

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE earthcheck_dbo.ChecklistRecord_AnswerHist;
        END;

        INSERT INTO earthcheck_dbo.ChecklistRecord_AnswerHist
            (
                checklist_record_id
              , answer_id
              , WH_RunGroup
            )
        SELECT cra.checklist_record_id
             , cra.answer_id
             , @RunGroup AS WH_RunGroup
          FROM [etl].[Transform_MemberChecklistRecordsHist] tmcr
              JOIN Earthcheck.[dbo].[ChecklistRecord_Answer] cra ON cra.checklist_record_id = tmcr.checklist_record_id;

		SET @RowCount = @@ROWCOUNT

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE()
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

			EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName 
							  , @Message = @Message

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

		SET @Message = 'Completed'
		EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName
							  , @RowCount = @RowCount
							  , @Message = @Message
		
END;
GO
