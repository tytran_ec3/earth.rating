SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Loading member details to EarthRating.dbo.Member
-- Modified:	23/11/2017	Updated status for unachieved 
-- =============================================
CREATE PROCEDURE [etl].[Load_EarthRating_etl_Member]
AS
BEGIN
    SET NOCOUNT ON;

    SELECT m.[member_id]
         , m.name [Member_Name]
         , m.Country
         , 1 [isactive] -- This is always true due to WHERE clause. 
		 , p.name [Product_Name]
		 ,CASE WHEN ggal.achievement_level_name IS NULL 
			THEN
				CASE WHEN p.name = 'Earthcheck' THEN 'Benchmarked Bronze'
					ELSE p.name END -- If not Certified that assign product name as achievement level.
				ELSE ggal.achievement_level_name END 
				AS [AchievementLevel]
      FROM ec3global_dbo.Member m
          JOIN ec3global_dbo.Member_Product mp ON mp.member_id = m.member_id
          LEFT JOIN ec3global_dbo.GreenGlobeProfile ggp ON ggp.member_id = m.member_id
		  LEFT JOIN ec3global_dbo.GreenGlobeStatus ggs ON ggs.status_id = ggp.status_id
		  LEFT JOIN ec3global_dbo.GreenGlobeAchievementHistory ggah ON ggah.member_id = m.member_id AND ggah.achievement_history_id = ggp.achievement_history_id
		  LEFT JOIN ec3global_dbo.GreenGlobeAchievementLevel ggal ON ggal.achievement_level_id = ggah.achievement_level_id
		  JOIN ec3global_dbo.Product p ON p.product_id = mp.product_id
     WHERE 
	 --NOT (
  --                 mp.product_id = 3
  --                 AND ggs.status_id IN (1,2) -- Benchmarking Bronze
  --             ) -- Jira: Move to config
  --         AND 
		   (
                   m.isactive = 1
                   AND mp.isactive = 1
               ); -- Only active members and member products

END;
GO
