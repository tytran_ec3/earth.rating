SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Get the latest checklist record to use in the Earth Rating transformation.
-- =============================================
CREATE PROCEDURE [etl].[Transform_GetChecklistRecordHist]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

        DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
              , @Message VARCHAR(MAX)
              , @RowCount INT;

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE [etl].[Transform_MemberChecklistRecordsHist];
        END;

        WITH cte
        AS (SELECT mp.member_id
                 , mp.product_id
                 , ce.checklist_episode_id
              FROM ec3global_dbo.Member_Product mp
                  JOIN earthcheck_dbo.ChecklistEpisode ce ON ce.member_id = mp.member_id -- AND ce.WH_RunGroup = mp.WH_RunGroup
           )
        INSERT INTO etl.Transform_MemberChecklistRecordsHist
            (
                member_id
              , product_id
              , LatestChecklistEpisode
              , checklist_record_id
              , checklist_id
              , version
              , WH_RunGroup
            )
        SELECT cte.member_id
             , cte.product_id
             , cte.checklist_episode_id
             , cr.checklist_record_id
             , cr.checklist_id
             , cr.version
             , @RunGroup AS [WH_RunGroup]
          FROM cte
              JOIN earthcheck_dbo.ChecklistRecord cr ON cr.member_id = cte.member_id
                                                        AND cr.checklist_episode_id = cte.checklist_episode_id
              JOIN earthcheck_dbo.Component_Checklist cc ON cc.checklist_id = cr.checklist_id
                                                            AND cc.version = cr.version
                                                            AND cte.product_id = cc.product_id
         ORDER BY cr.member_id
                , cr.checklist_record_id;

        SET @RowCount = @@ROWCOUNT;

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE();
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

        EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
                              , @ObjectName = @ObjectName
                              , @Message = @Message;

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

    SET @Message = 'Completed';
    EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
                          , @ObjectName = @ObjectName
                          , @RowCount = @RowCount
                          , @Message = @Message;
END;
GO
