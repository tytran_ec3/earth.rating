SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load Earthcheck.dbo.Checklist To EarthRating DB. 
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_Checklist]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    IF @Truncate = 1
    BEGIN
        TRUNCATE TABLE [earthcheck_dbo].[Checklist];
    END;

    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

        INSERT INTO [earthcheck_dbo].[Checklist]
            (
                [checklist_id]
              , [version]
              , [name]
              , [text]
              , [translation_required]
              , [checklist_rating_system_id]
              , [WH_RunGroup]
            )
        SELECT [checklist_id]
             , [version]
             , [name]
             , [text]
             , [translation_required]
             , [checklist_rating_system_id]
             , @RunGroup AS [WH_RunGroup]
          FROM [Earthcheck].dbo.[Checklist];

    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
