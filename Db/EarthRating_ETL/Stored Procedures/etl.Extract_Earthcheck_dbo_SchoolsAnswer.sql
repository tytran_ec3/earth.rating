SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Load Earthcheck.dbo.SchoolAnswer to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_SchoolsAnswer]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    IF @Truncate = 1
    BEGIN
        TRUNCATE TABLE [earthcheck_dbo].[SchoolsAnswer];
    END;

    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

        INSERT INTO earthcheck_dbo.SchoolsAnswer
            (
                answer_id
              , question_id
              , value
              , item_id
              , text_value
              , control_type_id
              , WH_RunGroup
            )
        SELECT sa.[answer_id]
             , sa.[question_id]
             , sa.[value]
             , sa.[item_id]
             , sa.[text_value]
             , sa.[control_type_id]
             , @RunGroup [WH_RunGroup]
          FROM earthcheck_dbo.ChecklistRecord_Answer cra
              JOIN Earthcheck.dbo.[SchoolsAnswer] sa ON sa.answer_id = cra.answer_id;
    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
