SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load EC3Global_dbo.Members to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_EC3Global_dbo_Member]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    IF @Truncate = 1
    BEGIN
        TRUNCATE TABLE [ec3global_dbo].[Member];
    END;

    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

        INSERT INTO ec3global_dbo.Member
            (
                member_id
              , name
              , Parent_id
              , standard_id
              , size_id
              , profile_id
              , isactive
              , ispublicview
              , employees
              , add_timestamp
              , status_timestamp
              , registration_date
              , sector_id
              , locale_id
              , Country_Id
              , Country
              , abn
              , benchmarking_review
              , deleted_timestamp
              , deleted_byuser
              , WH_RunGroup
            )
        SELECT m.[member_id]
             , m.[name]
             , NULL AS [Parent_id]
             , NULL AS [standard_id]
             , NULL AS [size_id]
             , NULL AS [profile_id]
             , m.[isactive]
             , NULL AS [ispublicview]
             , NULL AS [employees]
             , NULL AS [add_timestamp]
             , NULL AS [status_timestamp]
             , NULL AS [registration_date]
             , m.[sector_id]
             , m.[locale_id]
             , a.country_id
             , c.name_iso AS [Country]
             , NULL AS [abn]
             , NULL AS [benchmarking_review]
             , NULL AS [deleted_timestamp]
             , NULL AS [deleted_byuser]
             , @RunGroup AS [WH_RunGroup]
          FROM [EC3Global].[dbo].[Member] m
              JOIN ec3global_dbo.Member_Product mp ON mp.member_id = m.member_id
              JOIN EC3Global.dbo.MemberContact mc ON mc.member_id = m.member_id
              JOIN EC3Global.dbo.Address a ON a.address_id = mc.address_id
              JOIN EC3Global.dbo.Country c ON c.country_id = a.country_id
			  JOIN config.DatafeedMember dm ON dm.MemberId = m.member_id AND dm.Active = 1
    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
