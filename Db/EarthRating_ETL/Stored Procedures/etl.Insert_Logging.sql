SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [etl].[Insert_Logging]
(
    @RunGroupId INT
	, @ObjectName VARCHAR(MAX)
	, @RowCount INT = NULL
	, @Message VARCHAR(MAX)
)
AS
BEGIN
    INSERT INTO [etl].[Logging]
    (
        [RunGroupId], [ObjectName], [RowCount], [Message]
    )
    VALUES (@RunGroupId, @ObjectName, @RowCount, @Message);


END;
GO
