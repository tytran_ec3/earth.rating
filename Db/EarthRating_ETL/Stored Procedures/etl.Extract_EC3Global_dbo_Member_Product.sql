SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load EC3Global_dbo.Member_Products to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_EC3Global_dbo_Member_Product]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE [ec3global_dbo].[Member_Product];
        END;
		
        INSERT INTO ec3global_dbo.Member_Product
            (
                member_product_id
              , member_id
              , product_id
              , isactive
              , add_timestamp
              , renew_timestamp
              , first_payment_date
              , WH_RunGroup
            )
        SELECT mp.[member_product_id]
             , mp.[member_id]
             , mp.[product_id]
             , mp.[isactive]
             , NULL AS [add_timestamp]
             , NULL AS [renew_timestamp]
             , NULL AS [first_payment_date]
             , @RunGroup AS WH_RunGroup
          FROM [EC3Global].[dbo].[Member_Product] mp
              JOIN ec3global_dbo.Product p ON p.product_id = mp.product_id
    END TRY
    BEGIN CATCH

        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;



END;
GO
