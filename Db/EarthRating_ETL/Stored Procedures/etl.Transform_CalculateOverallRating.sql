SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Calculate the overall rating based on the individual checklist rating.
-- =============================================
CREATE PROCEDURE [etl].[Transform_CalculateOverallRating]
AS
BEGIN
    SET NOCOUNT ON;

    SELECT erf.member_id,
           CASE
               WHEN AVG(erf.Rating) >= 1 THEN
                   CAST(AVG(erf.Rating) AS INT)
               ELSE
                   1
           END 'Overall_Rating'
    FROM etl.Transform_MemberChecklistRating erf
    GROUP BY erf.member_id;
END;
GO
