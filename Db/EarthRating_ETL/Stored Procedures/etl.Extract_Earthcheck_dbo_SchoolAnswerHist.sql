SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Load Earthcheck.dbo.SchoolAnswer to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_SchoolAnswerHist]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

	DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	, @Message VARCHAR(MAX), @RowCount INT


        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE [earthcheck_dbo].[SchoolsAnswerHist];
        END;

        INSERT INTO earthcheck_dbo.SchoolsAnswerHist
            (
                answer_id
              , question_id
              , value
              , item_id
              , text_value
              , control_type_id
              , WH_RunGroup
            )
        SELECT sa.[answer_id]
             , sa.[question_id]
             , sa.[value]
             , sa.[item_id]
             , sa.[text_value]
             , sa.[control_type_id]
             , @RunGroup [WH_RunGroup]
          FROM earthcheck_dbo.ChecklistRecord_AnswerHist cra
              JOIN Earthcheck.dbo.[SchoolsAnswer] sa ON sa.answer_id = cra.answer_id;

		SET @RowCount = @@ROWCOUNT

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE()
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

			EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName 
							  , @Message = @Message

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

		SET @Message = 'Completed'
		EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName
							  , @RowCount = @RowCount
							  , @Message = @Message
END;
GO
