SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load Earthcheck.dbo.ComponentAccess To EarthRating DB. The extra joins is to reduce the amount of rows returned based on the products and members
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_ComponentAccess]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    IF @Truncate = 1
    BEGIN
        TRUNCATE TABLE [earthcheck_dbo].[ComponentAccess];
    END;

    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY
        INSERT INTO earthcheck_dbo.ComponentAccess
            (
                product_id
              , component_id
              , component_version
              , seq_num
              , sector_id
              , country_id
              , WH_RunGroup
            )
        SELECT DISTINCT
            ca.[product_id]
          , ca.[component_id]
          , ca.[component_version]
          , NULL AS [seq_num]
          , ca.[sector_id]
          , ca.[country_id]
          , @RunGroup AS [WH_RunGroup]
          FROM ec3global_dbo.Member m
              JOIN ec3global_dbo.Member_Product mp ON mp.member_id = m.member_id
              JOIN Earthcheck.[dbo].[ComponentAccess] ca ON ca.country_id = m.Country_Id
                                                            AND ca.sector_id = m.sector_id
                                                            AND mp.product_id = ca.product_id;

    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
