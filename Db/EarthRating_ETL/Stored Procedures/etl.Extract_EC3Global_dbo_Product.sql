SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ==========================================================================================
-- Type       Date              Author  Description
-- Created: 04/09/2017   TT         Load EC3Global_dbo.Product to EarthRating DB. Use etl.Config_IncludedProducts to add new products
-- Modified: 
-- ==========================================================================================
CREATE PROCEDURE [etl].[Extract_EC3Global_dbo_Product]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRAN;
    BEGIN TRY
        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE [ec3global_dbo].[Product];
        END;

        INSERT INTO ec3global_dbo.Product
            (
                product_id
              , name
              , rm_support
              , WH_RunGroup
            )
        SELECT p.[product_id]
             , p.[name]
             , NULL AS [rm_support]
             , @RunGroup AS WH_RunGroup
          FROM EC3Global.[dbo].[Product] p
              JOIN config.Product cp ON cp.Name COLLATE DATABASE_DEFAULT = p.name
              JOIN config.DatafeedProduct cdp ON cdp.ProductId = cp.ProductId
         WHERE cp.Active = 1;
    -- To do Intergrate to data feed.
    --WHERE cdp.DatafeedId = @DatafeedId AND cdp.Active = 1

    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

END;
GO
