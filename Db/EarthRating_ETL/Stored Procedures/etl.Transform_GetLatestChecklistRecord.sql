SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Get the latest checklist record to use in the Earth Rating transformation.
-- =============================================
CREATE PROCEDURE [etl].[Transform_GetLatestChecklistRecord] @RunGroup INT = NULL
AS
BEGIN
    SET NOCOUNT ON;
    WITH cte
    AS (SELECT mp.member_id
               , mp.product_id
               , (
                     SELECT TOP (1)
                         checklist_episode_id
                     FROM earthcheck_dbo.ChecklistEpisode ce
                     WHERE member_id = mp.member_id
                     ORDER BY checklist_episode_id DESC
                 ) AS [LatestChecklistEpisode]
        FROM ec3global_dbo.Member_Product mp
       )
    SELECT cte.member_id
           , cte.product_id
           , cte.LatestChecklistEpisode
           , cr.checklist_record_id
           , cr.checklist_id
           , cr.version
           , @RunGroup AS [WH_RunGroup]
    FROM cte
        JOIN earthcheck_dbo.ChecklistRecord cr ON cr.member_id = cte.member_id
                                                  AND cr.checklist_episode_id = cte.LatestChecklistEpisode
        JOIN earthcheck_dbo.Component_Checklist cc ON cc.checklist_id = cr.checklist_id
                                                      AND cc.version = cr.version
                                                      AND cte.product_id = cc.product_id;
END;
GO
