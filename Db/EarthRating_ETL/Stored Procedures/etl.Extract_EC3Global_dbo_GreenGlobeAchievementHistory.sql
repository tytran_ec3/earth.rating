SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Load EC3Global_dbo.GreenGlobeAchievementHistory to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_EC3Global_dbo_GreenGlobeAchievementHistory]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN
/* Begin Header */
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

	DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	, @Message VARCHAR(MAX), @RowCount INT

    IF @Truncate = 1
    BEGIN
        TRUNCATE TABLE [ec3global_dbo].[GreenGlobeAchievementHistory];
    END;
	/* End Header */


    INSERT INTO ec3global_dbo.GreenGlobeAchievementHistory
        (
            achievement_history_id
          , member_id
          , achievement_level_id
          , achievement_date
          , achievement_expiry_date
          , admin_user_id
          , admin_comment
          , add_timestamp
          , not_public
          , WH_RunGroup
        )
       
    SELECT ggah.achievement_history_id
         , ggah.member_id
         , ggah.achievement_level_id
         , ggah.achievement_date
         , ggah.achievement_expiry_date
         , NULL AS admin_user_id
         , NULL AS admin_comment
         , ggah.add_timestamp
         , NULL AS not_public
		 , @RunGroup
        FROM EC3Global.[dbo].GreenGlobeAchievementHistory ggah
		JOIN ec3global_dbo.Member_Product mp ON mp.member_id = ggah.member_id

	/* Begin Footer */
	SET @RowCount = @@ROWCOUNT

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE()
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

			EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName 
							  , @Message = @Message

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

		SET @Message = 'Completed'
		EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName
							  , @RowCount = @RowCount
							  , @Message = @Message
	/* End Header */
END;
GO
