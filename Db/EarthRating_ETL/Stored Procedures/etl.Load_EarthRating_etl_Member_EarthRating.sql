SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ==========================================================================================
-- Type			Date		Author	Description
-- Created:		05/09/2017	Ty		Loading member details to EarthRating.dbo.Member_EarthRating
-- Modified:	26/09/2017	Ty		Updated to a MERGE Statement to allow for updates.
-- ==========================================================================================

CREATE PROCEDURE [etl].[Load_EarthRating_etl_Member_EarthRating]
AS
BEGIN
    SET NOCOUNT ON;

    SELECT tmcr.member_id,
           tmcr.checklist_id,
           CAST(SUBSTRING(c.name, CHARINDEX(' ', c.name) + 1, LEN(c.name)) AS NVARCHAR(100)) [ChecklistName],
           CAST(tmcr.Rating AS TINYINT) 'EarthRating'
    FROM etl.Transform_MemberChecklistRating tmcr
        JOIN earthcheck_dbo.Checklist c ON c.checklist_id = tmcr.checklist_id;

END;
GO
