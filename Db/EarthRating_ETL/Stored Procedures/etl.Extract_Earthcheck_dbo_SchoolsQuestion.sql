SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load Earthcheck.dbo.SchoolsQuestion to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_SchoolsQuestion]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRAN;
    BEGIN TRY

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE [earthcheck_dbo].[SchoolsQuestion];
        END;

        INSERT INTO earthcheck_dbo.SchoolsQuestion
            (
                question_id
              , question_text
              , control_type_id
              , seq_nr
              , translation_required
              , is_deleted
              , is_disabled
              , parent_id
              , is_hidden
              , WH_RunGroup
            )
        SELECT [question_id]
             , [question_text]
             , [control_type_id]
             , [seq_nr]
             , [translation_required]
             , [is_deleted]
             , [is_disabled]
             , [parent_id]
             , [is_hidden]
             , @RunGroup [WH_RunGroup]
          FROM [Earthcheck].[dbo].[SchoolsQuestion];

    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
