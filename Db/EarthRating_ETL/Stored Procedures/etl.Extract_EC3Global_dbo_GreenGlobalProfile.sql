SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Load EC3Global_dbo.GreenGlobalProfile to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_EC3Global_dbo_GreenGlobalProfile]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

	DECLARE @ObjectName VARCHAR(MAX) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	, @Message VARCHAR(MAX), @RowCount INT

    IF @Truncate = 1
    BEGIN
        TRUNCATE TABLE [ec3global_dbo].[GreenGlobalProfile];
    END;


    INSERT INTO ec3global_dbo.GreenGlobeProfile
        (
            profile_id
            , member_id
            , entry_level_id
            , status_id
            , achievement_history_id
            , WH_RunGroup
        )
    SELECT ggp.[profile_id]
            , ggp.[member_id]
            , NULL AS [entry_level_id]
            , ggp.[status_id]
            , ggp.achievement_history_id
            , @RunGroup WH_ExtractGroup
        FROM EC3Global.[dbo].[GreenGlobeProfile] ggp
		JOIN ec3global_dbo.Member_Product mp ON mp.member_id = ggp.member_id

	SET @RowCount = @@ROWCOUNT

    END TRY
    BEGIN CATCH
        SET @Message = ERROR_PROCEDURE() + ' ' + ERROR_MESSAGE()
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

			EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName 
							  , @Message = @Message

    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;

		SET @Message = 'Completed'
		EXEC etl.Insert_Logging @RunGroupId = @RunGroup -- int
		                      , @ObjectName = @ObjectName
							  , @RowCount = @RowCount
							  , @Message = @Message

END;
GO
