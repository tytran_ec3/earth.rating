SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load Earthcheck.dbo.ChecklistEpisode to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_ChecklistEpisode]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    IF @Truncate = 1
    BEGIN
        TRUNCATE TABLE [earthcheck_dbo].[ChecklistEpisode];
    END;

    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY
        INSERT INTO earthcheck_dbo.ChecklistEpisode
            (
                checklist_episode_id
              , add_timestamp
              , member_id
              , submission_timestamp
              , WH_RunGroup
            )
        SELECT ce.checklist_episode_id
             , NULL [add_timestamp]
             , ce.member_id
             , NULL [submission_timestamp]
             , @RunGroup AS [WH_RunGroup]
          FROM ec3global_dbo.Member_Product mp
              JOIN Earthcheck.dbo.ChecklistEpisode ce ON ce.member_id = mp.member_id;

    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
