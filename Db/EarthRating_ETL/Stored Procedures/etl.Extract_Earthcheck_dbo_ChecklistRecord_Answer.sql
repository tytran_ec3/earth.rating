SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load Earthcheck.dbo.ChecklistRecord_Answer to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_ChecklistRecord_Answer]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    IF @Truncate = 1
    BEGIN
        TRUNCATE TABLE [earthcheck_dbo].[ChecklistRecord_Answer];
    END;
    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

        INSERT INTO earthcheck_dbo.ChecklistRecord_Answer
            (
                checklist_record_id
              , answer_id
              , WH_ExtractGroup
            )
        SELECT cra.checklist_record_id
             , cra.answer_id
             , GETDATE() AS WH_ExtractGroup
          FROM Earthcheck.[dbo].[ChecklistRecord_Answer] cra
              JOIN [earthcheck_dbo].[ChecklistRecord] cr ON cr.checklist_record_id = cra.checklist_record_id
              JOIN [etl].[Transform_MemberChecklistRecords] tmcr ON tmcr.[checklist_record_id] = cra.checklist_record_id;


    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
