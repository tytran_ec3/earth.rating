SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		TT
-- Create date: 04/09/2017
-- Description:	Load Earthcheck.dbo.Section_Question to EarthRating DB.
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_Section_Question]
    @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;


    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

        IF @Truncate = 1
        BEGIN
            TRUNCATE TABLE [earthcheck_dbo].[Section_Question];
        END;

        INSERT INTO earthcheck_dbo.Section_Question
            (
                section_id
              , question_id
              , seq_nr
              , mandatory
              , is_commentable
              , attachment
              , WH_RunGroup
            )
        SELECT [section_id]
             , [question_id]
             , [seq_nr]
             , [mandatory]
             , [is_commentable]
             , [attachment]
             , @RunGroup [WH_RunGroup]
          FROM [Earthcheck].[dbo].[Section_Question];

    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
