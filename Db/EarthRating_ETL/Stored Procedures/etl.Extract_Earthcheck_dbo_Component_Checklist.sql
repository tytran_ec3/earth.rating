SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		TT
-- Create date: 05/09/2017
-- Description:	Load Earthcheck.dbo.Component_Checklist to EarthRating DB. 
--              @RatingSysId is from Earthcheck.dbo.ChecklistRatingSystem
-- Modified: 16/10/2017 Add Dynamic SQL and RunGroup
-- =============================================
CREATE PROCEDURE [etl].[Extract_Earthcheck_dbo_Component_Checklist]
    @RatingSysId INT = NULL
  , @RunGroup INT = NULL
  , @Truncate BIT = NULL
AS
BEGIN

    SET NOCOUNT ON;
    IF @Truncate = 1
    BEGIN
        TRUNCATE TABLE [earthcheck_dbo].[Component_Checklist];
    END;

    SET XACT_ABORT ON;
    BEGIN TRAN;
    BEGIN TRY

        DECLARE @SqlCmd VARCHAR(MAX);

        SELECT @SqlCmd
            = N'
			INSERT INTO earthcheck_dbo.Component_Checklist
		    (
		        component_id
		      , component_version
		      , checklist_id
		      , version
		      , product_id
		      , WH_RunGroup
		    )
    SELECT DISTINCT
        cc.[component_id]
        , cc.[component_version]
        , cc.[checklist_id]
        , cc.[version]
        , ca.product_id
        , ' + CAST(ISNULL(@RunGroup, '') AS NVARCHAR)
              + ' AS [WH_RunGroup]
    FROM Earthcheck.[dbo].[Component_Checklist] cc
        JOIN earthcheck_dbo.ComponentAccess ca ON ca.component_id = cc.component_id
                                                  AND ca.component_version = cc.component_version
        JOIN earthcheck_dbo.Checklist c ON c.checklist_id = cc.checklist_id
                                           AND c.version = cc.version'
              + CASE
                    WHEN @RatingSysId IS NOT NULL THEN
                        +' AND c.checklist_rating_system_id = ' + CAST(@RatingSysId AS NVARCHAR)
                    ELSE
                        ''
                END;
        EXECUTE (@SqlCmd);

    -- Debugging Code
    -- PRINT @SqlCmd;

    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
             , ERROR_SEVERITY() AS ErrorSeverity
             , ERROR_STATE() AS ErrorState
             , ERROR_PROCEDURE() AS ErrorProcedure
             , ERROR_LINE() AS ErrorLine
             , ERROR_MESSAGE() AS ErrorMessage;
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;
    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
END;
GO
