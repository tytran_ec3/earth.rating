CREATE TABLE [config].[Achievement]
(
[AchievementId] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[Name] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [config].[Achievement] ADD CONSTRAINT [PK_Achievement] PRIMARY KEY CLUSTERED  ([AchievementId], [ProductId]) ON [PRIMARY]
GO
