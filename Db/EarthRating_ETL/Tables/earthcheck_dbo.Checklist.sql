CREATE TABLE [earthcheck_dbo].[Checklist]
(
[checklist_id] [int] NOT NULL,
[version] [int] NULL,
[name] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[text] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[translation_required] [bit] NULL,
[checklist_rating_system_id] [int] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[Checklist] ADD CONSTRAINT [PK_earthcheck_dbo_Checklist] PRIMARY KEY CLUSTERED  ([WH_Id]) ON [PRIMARY]
GO
