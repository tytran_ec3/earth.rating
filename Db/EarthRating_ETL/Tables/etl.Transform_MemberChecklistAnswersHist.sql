CREATE TABLE [etl].[Transform_MemberChecklistAnswersHist]
(
[member_id] [int] NOT NULL,
[checklist_episode_id] [int] NOT NULL,
[checklist_id] [int] NOT NULL,
[TotalYesAnswers] [int] NULL,
[TotalNoAnswers] [int] NULL,
[TotalNaAnswers] [int] NULL,
[TotalQuestions] [int] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
