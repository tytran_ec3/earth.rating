CREATE TABLE [ec3global_dbo].[GreenGlobeAchievementHistory]
(
[achievement_history_id] [int] NOT NULL,
[member_id] [int] NOT NULL,
[achievement_level_id] [int] NULL,
[achievement_date] [datetime] NULL,
[achievement_expiry_date] [datetime] NULL,
[admin_user_id] [nvarchar] (128) COLLATE Latin1_General_CI_AS NULL,
[admin_comment] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[add_timestamp] [datetime] NULL,
[not_public] [bit] NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ec3global_dbo].[GreenGlobeAchievementHistory] ADD CONSTRAINT [PK_ec3global_dbo_GreenGlobeAchievementHistory] PRIMARY KEY NONCLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
