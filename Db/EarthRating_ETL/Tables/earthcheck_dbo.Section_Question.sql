CREATE TABLE [earthcheck_dbo].[Section_Question]
(
[section_id] [int] NOT NULL,
[question_id] [int] NOT NULL,
[seq_nr] [int] NOT NULL,
[mandatory] [bit] NOT NULL,
[is_commentable] [bit] NOT NULL,
[attachment] [bit] NOT NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[Section_Question] ADD CONSTRAINT [PK_earthcheck_dbo_Section_Question] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
