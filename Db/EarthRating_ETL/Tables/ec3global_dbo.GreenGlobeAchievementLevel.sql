CREATE TABLE [ec3global_dbo].[GreenGlobeAchievementLevel]
(
[achievement_level_id] [int] NOT NULL,
[achievement_level_name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[translation_required] [bit] NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ec3global_dbo].[GreenGlobeAchievementLevel] ADD CONSTRAINT [PK_ec3global_dbo_GreenGlobeAchievementLevel] PRIMARY KEY NONCLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
