CREATE TABLE [earthcheck_dbo].[ChecklistEpisode]
(
[checklist_episode_id] [int] NOT NULL,
[add_timestamp] [datetime] NULL,
[member_id] [int] NOT NULL,
[submission_timestamp] [datetime] NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[ChecklistEpisode] ADD CONSTRAINT [PK_earthcheck_dbo_ChecklistEpisode] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
