CREATE TABLE [earthcheck_dbo].[ChecklistRecord_Answer]
(
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[checklist_record_id] [int] NOT NULL,
[answer_id] [int] NOT NULL,
[WH_ExtractGroup] [datetime] NOT NULL,
[WH_Rowversion] [timestamp] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[ChecklistRecord_Answer] ADD CONSTRAINT [PK_earthcheck_dbo_ChecklistRecord_Answer] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
