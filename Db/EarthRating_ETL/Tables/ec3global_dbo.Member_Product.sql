CREATE TABLE [ec3global_dbo].[Member_Product]
(
[member_product_id] [int] NOT NULL,
[member_id] [int] NULL,
[product_id] [int] NULL,
[isactive] [bit] NULL,
[add_timestamp] [datetime] NULL,
[renew_timestamp] [datetime] NULL,
[first_payment_date] [datetime] NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ec3global_dbo].[Member_Product] ADD CONSTRAINT [PK_ec3global_dbo_Member_Product] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
