CREATE TABLE [config].[Datafeed]
(
[DatafeedId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [config].[Datafeed] ADD CONSTRAINT [PK_Datafeed] PRIMARY KEY CLUSTERED  ([DatafeedId]) ON [PRIMARY]
GO
