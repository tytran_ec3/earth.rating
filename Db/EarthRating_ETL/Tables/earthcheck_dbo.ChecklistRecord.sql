CREATE TABLE [earthcheck_dbo].[ChecklistRecord]
(
[checklist_record_id] [int] NOT NULL,
[checklist_id] [int] NULL,
[version] [int] NULL,
[member_id] [int] NULL,
[add_timestamp] [datetime] NULL,
[submission_timestamp] [datetime] NULL,
[checklist_episode_id] [int] NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[ChecklistRecord] ADD CONSTRAINT [PK_earthcheck_dbo_ChecklistRecord] PRIMARY KEY CLUSTERED  ([checklist_record_id]) ON [PRIMARY]
GO
