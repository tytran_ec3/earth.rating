CREATE TABLE [earthcheck_dbo].[SchoolsAnswer]
(
[answer_id] [int] NOT NULL,
[question_id] [int] NOT NULL,
[value] [decimal] (38, 10) NOT NULL,
[item_id] [int] NULL,
[text_value] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[control_type_id] [int] NOT NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[SchoolsAnswer] ADD CONSTRAINT [PK_earthcheck_dbo_SchoolsAnswer] PRIMARY KEY NONCLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
