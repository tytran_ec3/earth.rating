CREATE TABLE [etl].[Logging]
(
[LoggingId] [int] NOT NULL IDENTITY(1, 1),
[RunGroupId] [int] NOT NULL,
[DateTime] [datetime] NOT NULL CONSTRAINT [DF__Logging__DateTim__05A3D694] DEFAULT (getdate()),
[ObjectName] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Message] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[RowCount] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [etl].[Logging] ADD CONSTRAINT [PK_Logging] PRIMARY KEY CLUSTERED  ([LoggingId]) ON [PRIMARY]
GO
