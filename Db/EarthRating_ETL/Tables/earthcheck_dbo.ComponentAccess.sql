CREATE TABLE [earthcheck_dbo].[ComponentAccess]
(
[product_id] [int] NULL,
[component_id] [int] NULL,
[component_version] [int] NULL,
[seq_num] [int] NULL,
[sector_id] [int] NULL,
[country_id] [int] NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[ComponentAccess] ADD CONSTRAINT [PK_earthcheck_dbo_ComponentAccess] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
