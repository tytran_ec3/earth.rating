CREATE TABLE [etl].[Transform_MemberChecklistRatingBySectionHist]
(
[member_id] [int] NULL,
[checklist_episode_id] [int] NULL,
[checklist_id] [int] NULL,
[TotalQuestions] [int] NULL,
[SubSection] [int] NULL,
[TotalNaAnswers] [int] NULL,
[TotalYesAnswers] [int] NULL,
[EarthRating] [tinyint] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
