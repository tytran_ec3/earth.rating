CREATE TABLE [config].[DatafeedProduct]
(
[DatafeedProductId] [int] NOT NULL IDENTITY(1, 1),
[DataFeedId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [config].[DatafeedProduct] ADD CONSTRAINT [PK_DatafeedProduct] PRIMARY KEY CLUSTERED  ([DatafeedProductId]) ON [PRIMARY]
GO
