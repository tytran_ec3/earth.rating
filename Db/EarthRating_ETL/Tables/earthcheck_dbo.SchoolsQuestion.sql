CREATE TABLE [earthcheck_dbo].[SchoolsQuestion]
(
[question_id] [int] NOT NULL,
[question_text] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL,
[control_type_id] [int] NOT NULL,
[seq_nr] [int] NOT NULL,
[translation_required] [bit] NOT NULL,
[is_deleted] [bit] NOT NULL,
[is_disabled] [bit] NOT NULL,
[parent_id] [int] NULL,
[is_hidden] [bit] NOT NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[SchoolsQuestion] ADD CONSTRAINT [PK_earthcheck_dbo_SchoolsQuestion] PRIMARY KEY NONCLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
