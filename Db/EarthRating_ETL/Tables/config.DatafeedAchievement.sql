CREATE TABLE [config].[DatafeedAchievement]
(
[DataFeedId] [int] NOT NULL,
[AchievementId] [int] NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [config].[DatafeedAchievement] ADD CONSTRAINT [PK_DatafeedAchievement] PRIMARY KEY CLUSTERED  ([DataFeedId], [AchievementId]) ON [PRIMARY]
GO
