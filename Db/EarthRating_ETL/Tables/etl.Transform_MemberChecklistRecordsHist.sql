CREATE TABLE [etl].[Transform_MemberChecklistRecordsHist]
(
[member_id] [int] NULL,
[product_id] [int] NULL,
[LatestChecklistEpisode] [int] NULL,
[checklist_record_id] [int] NULL,
[checklist_id] [int] NULL,
[version] [int] NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
