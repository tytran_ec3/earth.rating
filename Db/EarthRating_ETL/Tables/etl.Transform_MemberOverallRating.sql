CREATE TABLE [etl].[Transform_MemberOverallRating]
(
[Member_Id] [int] NULL,
[Overall_Rating] [int] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
