CREATE TABLE [etl].[Transform_MemberChecklistAnswers]
(
[member_id] [int] NULL,
[checklist_id] [int] NULL,
[TotalYesAnswers] [int] NULL,
[TotalNoAnswers] [int] NULL,
[TotalNaAnswers] [int] NULL,
[TotalQuestions] [int] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_ExtractGroup] [datetime] NOT NULL,
[WH_Rowversion] [timestamp] NOT NULL
) ON [PRIMARY]
GO
