CREATE TABLE [ec3global_dbo].[GreenGlobeProfile]
(
[profile_id] [int] NOT NULL,
[member_id] [int] NOT NULL,
[entry_level_id] [int] NULL,
[status_id] [int] NULL,
[achievement_history_id] [int] NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ec3global_dbo].[GreenGlobeProfile] ADD CONSTRAINT [PK_ec3global_dbo_GreenGlobeProfile] PRIMARY KEY NONCLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
