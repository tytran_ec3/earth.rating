CREATE TABLE [earthcheck_dbo].[SchoolsItem_Gbl]
(
[item_id] [int] NOT NULL,
[locale_id] [int] NOT NULL,
[item_text] [nvarchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[item_text_is_translated] [bit] NOT NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[SchoolsItem_Gbl] ADD CONSTRAINT [PK_earthcheck_dbo_SchoolsItem_Gbl] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
