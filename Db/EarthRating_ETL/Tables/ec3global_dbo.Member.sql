CREATE TABLE [ec3global_dbo].[Member]
(
[member_id] [int] NOT NULL,
[name] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Parent_id] [int] NULL,
[standard_id] [int] NULL,
[size_id] [int] NULL,
[profile_id] [int] NULL,
[isactive] [bit] NOT NULL,
[ispublicview] [bit] NULL,
[employees] [int] NULL,
[add_timestamp] [datetime] NULL,
[status_timestamp] [datetime] NULL,
[registration_date] [datetime] NULL,
[sector_id] [int] NULL,
[locale_id] [int] NULL,
[Country_Id] [int] NOT NULL,
[Country] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[abn] [char] (11) COLLATE Latin1_General_CI_AS NULL,
[benchmarking_review] [bit] NULL,
[deleted_timestamp] [datetime] NULL,
[deleted_byuser] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ec3global_dbo].[Member] ADD CONSTRAINT [PK_ec3global_dbo_Member] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
