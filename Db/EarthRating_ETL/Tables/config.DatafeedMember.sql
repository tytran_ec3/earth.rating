CREATE TABLE [config].[DatafeedMember]
(
[DatafeedId] [int] NOT NULL,
[MemberId] [int] NOT NULL,
[Active] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [config].[DatafeedMember] ADD CONSTRAINT [PK_config_DatafeedMember_DatafeedId_Member_Id] PRIMARY KEY CLUSTERED  ([DatafeedId], [MemberId]) ON [PRIMARY]
GO
