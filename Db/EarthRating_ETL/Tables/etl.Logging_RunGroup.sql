CREATE TABLE [etl].[Logging_RunGroup]
(
[RunGroupId] [bigint] NOT NULL IDENTITY(1, 1),
[StartDateTime] [datetime] NOT NULL,
[EndDateTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [etl].[Logging_RunGroup] ADD CONSTRAINT [PK_Logging_RunGroup] PRIMARY KEY CLUSTERED  ([RunGroupId]) ON [PRIMARY]
GO
