CREATE TABLE [earthcheck_dbo].[Component_Checklist]
(
[component_id] [int] NULL,
[component_version] [int] NULL,
[checklist_id] [int] NULL,
[version] [int] NULL,
[product_id] [int] NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[Component_Checklist] ADD CONSTRAINT [PK_earthcheck_dbo_Component_Checklist] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
