CREATE TABLE [etl].[Transform_MemberChecklistAnswersBySectionHist]
(
[member_id] [int] NULL,
[checklist_episode_id] [int] NULL,
[checklist_id] [int] NULL,
[section_id] [int] NULL,
[TotalYesAnswers] [int] NULL,
[TotalNoAnswers] [int] NULL,
[TotalNaAnswers] [int] NULL,
[TotalQuestions] [int] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
