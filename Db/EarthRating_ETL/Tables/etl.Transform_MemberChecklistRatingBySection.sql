CREATE TABLE [etl].[Transform_MemberChecklistRatingBySection]
(
[member_id] [int] NULL,
[checklist_id] [int] NULL,
[TotalQuestions] [int] NULL,
[SubSection] [int] NULL,
[TotalNaAnswers] [int] NULL,
[TotalYesAnswers] [int] NULL,
[EarthRating] [tinyint] NULL,
[WH_ExtractGroup] [datetime] NULL
) ON [PRIMARY]
GO
