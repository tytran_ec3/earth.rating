CREATE TABLE [ec3global_dbo].[GreenGlobeStatus]
(
[status_id] [int] NOT NULL,
[status_name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ec3global_dbo].[GreenGlobeStatus] ADD CONSTRAINT [PK_ec3global_dbo_GreenGlobeStatus] PRIMARY KEY NONCLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
