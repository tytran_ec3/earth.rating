CREATE TABLE [config].[Product]
(
[ProductId] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [config].[Product] ADD CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED  ([ProductId]) ON [PRIMARY]
GO
