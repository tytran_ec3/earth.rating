CREATE TABLE [etl].[Transform_MemberChecklistRating]
(
[member_id] [int] NULL,
[checklist_id] [int] NULL,
[Rating] [numeric] (17, 13) NULL,
[AchievementLevel] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
