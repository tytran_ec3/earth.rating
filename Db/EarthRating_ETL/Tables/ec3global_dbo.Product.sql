CREATE TABLE [ec3global_dbo].[Product]
(
[product_id] [int] NOT NULL,
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[rm_support] [bit] NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [ec3global_dbo].[Product] ADD CONSTRAINT [PK_ec3global_dbo_Product] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
