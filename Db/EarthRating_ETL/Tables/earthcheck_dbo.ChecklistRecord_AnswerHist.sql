CREATE TABLE [earthcheck_dbo].[ChecklistRecord_AnswerHist]
(
[checklist_record_id] [int] NOT NULL,
[answer_id] [int] NOT NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[ChecklistRecord_AnswerHist] ADD CONSTRAINT [PK_earthcheck_dbo_ChecklistRecord_AnswerHist] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
