CREATE TABLE [earthcheck_dbo].[Checklist_Section]
(
[checklist_id] [int] NOT NULL,
[version] [int] NOT NULL,
[section_id] [int] NOT NULL,
[seq_nr] [int] NOT NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[Checklist_Section] ADD CONSTRAINT [PK_earthcheck_dbo_Checklist_Section] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
