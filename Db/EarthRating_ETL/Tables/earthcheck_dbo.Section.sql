CREATE TABLE [earthcheck_dbo].[Section]
(
[section_id] [int] NOT NULL,
[name] [nvarchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL,
[text] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[translation_required] [bit] NOT NULL,
[WH_ID] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [earthcheck_dbo].[Section] ADD CONSTRAINT [PK_earthcheck_dbo_Section] PRIMARY KEY CLUSTERED  ([WH_ID]) ON [PRIMARY]
GO
