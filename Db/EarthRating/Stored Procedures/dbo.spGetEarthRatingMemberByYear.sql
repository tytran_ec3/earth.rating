SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- ==========================================================================================
-- Type			Date		Author  Description
-- Created:		01/10/2017	TT		Get Member Earth ratings. Used by EarthRating API. /Members & /Member 
-- Modified:	07/11/2017	TT		Remove @PrevYear From Results
-- Modified:	16/11/2017  TT		Added Member exist checks and error return
-- ==========================================================================================

CREATE PROCEDURE [dbo].[spGetEarthRatingMemberByYear]
    @MemberGuid UNIQUEIDENTIFIER
  , @PrevYear TINYINT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @err_message nvarchar(255)

	IF NOT EXISTS(SELECT TOP(1) * FROM dbo.Member_Guid WHERE Member_Guid = @MemberGuid)
	BEGIN
	SET @err_message = 'Member ' + CAST(@MemberGuid AS VARCHAR(40)) + ' not found.'
				 RAISERROR (@err_message,17, 1) 
	END	

    DECLARE @Achievement NVARCHAR(50);
    SELECT @Achievement = ISNULL(AchievementLevel,'')
      FROM dbo.Member m
	  JOIN Member_Guid mg ON mg.Member_Id = m.Member_Id
     WHERE mg.Member_Guid = @MemberGuid;

   
        IF (@Achievement LIKE 'Certifying%' AND @PrevYear IN ( 1, 2 ))
        BEGIN

            SELECT @MemberGuid [Member_Guid]
                 , m.Member_Name
                 , m.Country
                 , m.IsActive
                 , m.AchievementLevel
                 , 5 [EarthRating]
                 , 5 [Commitment]
                 , 5 [Governance]
                 , 5 [RiskManagement]
                 , 5 [Performance]
                 , 5 [Communication]
                 , m.LastUpdated
              FROM dbo.vw_Member_EarthRating_Hist m
             WHERE m.Member_Guid = @MemberGuid;
        /* Jira: Check if certified the previous year */
        END;
        ELSE --IF (@Achievement LIKE 'EarthCheck Evaluate%' AND @PrevYear IN ( 1, 2 )) --
			BEGIN
				SELECT @MemberGuid [Member_Guid]
					 , er.Member_Name
					 , er.Country
					 , er.IsActive
					 , er.AchievementLevel
					 , er.EarthRating
					 , er.Commitment
					 , er.Governance
					 , er.RiskManagement
					 , er.Performance
					 , er.Communication
					 , er.LastUpdated
				  FROM dbo.vw_Member_EarthRating_Hist er
				 WHERE er.Member_Guid = @MemberGuid
				 AND er.YearBy = @PrevYear
			/* Jira: Get episode */
			END;
END;
GO
