SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==========================================================================================
-- Type			Date		Author	Description
-- Created:		05/09/2017	Ty		Loading member details to EarthRating.dbo.Member
-- Modified:	26/09/2017	Ty		Updated to a MERGE Statement to allow for updates.
-- ==========================================================================================

CREATE PROCEDURE [etl].[Load_dbo_Member]
AS
BEGIN
    SET NOCOUNT ON;
    MERGE dbo.Member t
    USING etl.Member s
       ON (s.Member_Id = t.Member_Id)
     WHEN MATCHED AND (
                             ISNULL(s.Member_Name,'') <> ISNULL(t.Member_Name,'')
                          OR ISNULL(s.Country,'') <> ISNULL(t.Country,'')
                          OR ISNULL(s.IsActive,'') <> ISNULL(t.IsActive,'')
						  OR ISNULL(s.Product,'') <> ISNULL(t.Product,'')
                          OR ISNULL(s.AchievementLevel,'') <> ISNULL(t.AchievementLevel,'')
                      ) THEN
        UPDATE SET t.Member_Name = s.Member_Name
                 , t.Country = s.Country
                 , t.IsActive = s.IsActive
				 , t.Product = s.Product
                 , t.AchievementLevel = s.AchievementLevel
		 
     WHEN NOT MATCHED THEN -- Insert new members from ETL
        INSERT
            (
                Member_Id
              , Member_Name
              , Country
              , IsActive
			  , Product
              , AchievementLevel
            )
        VALUES
            (s.Member_Id, s.Member_Name, s.Country, s.IsActive, s.Product, s.AchievementLevel)
     WHEN NOT MATCHED BY SOURCE THEN
        UPDATE SET t.IsActive = 0 -- Disable members not in ETL
    ;
END;
GO
