SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ==========================================================================================
-- Type			Date		Author  Description
-- Created:		23/10/2017	TT		Get Member Earth ratings Key Performance Areas Historical Data. Used by EarthRating API.
-- Modified:	07/11/2017	TT		Remove @PrevYear From Results
-- Modified:	13/11/2017	TT		Add Certified @PrevYear Logic
-- Modified:	15/11/2017  GJ		Removed GUID from results.
-- Modified:	16/11/2017  TT		Added Member exist checks and error return
-- ==========================================================================================

CREATE PROCEDURE [dbo].[spGetEarthRatingMemberKpaByYear] @MemberGuid UNIQUEIDENTIFIER, @PrevYear INT
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @err_message NVARCHAR(255)
  IF NOT EXISTS(SELECT TOP(1) * FROM dbo.Member_Guid WHERE Member_Guid = @MemberGuid)
	BEGIN
	SET @err_message = 'Member ' + CAST(@MemberGuid AS VARCHAR(40)) + ' not found.'
				 RAISERROR (@err_message,17, 1) 
	END	

	IF ((SELECT TOP 1 erh.Year
       FROM dbo.vw_Member_EarthRating_Kpa_Hist erh 
       WHERE erh.Member_Guid = @MemberGuid) > 0) -- If Evaluate then pass through values based on year
	BEGIN
		SELECT --//erh.Member_Guid, 
           erh.GreenhouseGasEmissions, erh.EnergyEfficiencyConservationAndMgt, erh.MgtOfFreshwaterResources, erh.EcosystemConservationAndMgt, erh.SocialAndCulturalMgt,  
           erh.LandUsePlanningAndMgt, erh.AirQualityProtection, erh.WasteWaterMgt, erh.SolidWasteMgt, erh.EnviroHarmfulSubstances 
    FROM dbo.vw_Member_EarthRating_Kpa_Hist erh
		WHERE erh.Member_Guid = @MemberGuid
		AND erh.[Year] = @PrevYear
	END
	ELSE IF ((SELECT TOP 1 erh.Year
            FROM dbo.vw_Member_EarthRating_Kpa_Hist erh 
			      WHERE erh.Member_Guid = @MemberGuid) = 0) -- If Certified pass through with year. TODO: see certified historical todo list
	     BEGIN
	         SELECT --//erh.Member_Guid,
			            erh.GreenhouseGasEmissions, erh.EnergyEfficiencyConservationAndMgt, erh.MgtOfFreshwaterResources, erh.EcosystemConservationAndMgt, 
                  erh.SocialAndCulturalMgt, erh.LandUsePlanningAndMgt, erh.AirQualityProtection, erh.WasteWaterMgt, erh.SolidWasteMgt, erh.EnviroHarmfulSubstances 
           FROM dbo.vw_Member_EarthRating_Kpa_Hist erh
		       WHERE erh.Member_Guid = @MemberGuid
	     END
END;
GO
