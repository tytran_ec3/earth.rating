SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==========================================================================================
-- Type			Date		Author	Description
-- Created:		05/09/2017	Ty		Loading EarthRating Key Performance Area to EarthRating.dbo.Member_KeyPerformance
-- Modified:	26/09/2017	Ty		Updated to a MERGE Statement to allow for updates.
-- ==========================================================================================

CREATE PROCEDURE [etl].[Load_dbo_Member_KeyPerformance]
AS
BEGIN
    SET NOCOUNT ON;

    MERGE dbo.Member_KeyPerformance t
    USING etl.Member_KeyPerformance s
    ON (
           s.Member_Id = t.Member_Id
           AND s.Checklist_Id = t.Checklist_Id
       )
    WHEN MATCHED AND (
                         s.GreenhouseGasEmissions <> t.GreenhouseGasEmissions
                         AND s.EnergyEfficiencyConservationAndMgt <> t.EnergyEfficiencyConservationAndMgt
                         AND s.MgtOfFreshwaterResources <> t.MgtOfFreshwaterResources
                         AND s.EcosystemConservationAndMgt <> t.EcosystemConservationAndMgt
                         AND s.SocialAndCulturalMgt <> t.SocialAndCulturalMgt
                         AND s.LandUsePlanningAndMgt <> t.LandUsePlanningAndMgt
                         AND s.AirQualityProtection <> t.AirQualityProtection
                         AND s.WasteWaterMgt <> t.WasteWaterMgt
                         AND s.SolidWasteMgt <> t.SolidWasteMgt
                         AND s.EnviroHarmfulSubstances <> t.EnviroHarmfulSubstances
                     ) THEN
        UPDATE SET t.GreenhouseGasEmissions = s.GreenhouseGasEmissions
                   , t.EnergyEfficiencyConservationAndMgt = s.EnergyEfficiencyConservationAndMgt
                   , t.MgtOfFreshwaterResources = s.MgtOfFreshwaterResources
                   , t.EcosystemConservationAndMgt = s.EcosystemConservationAndMgt
                   , t.SocialAndCulturalMgt = s.SocialAndCulturalMgt
                   , t.LandUsePlanningAndMgt = s.LandUsePlanningAndMgt
                   , t.AirQualityProtection = s.AirQualityProtection
                   , t.WasteWaterMgt = s.WasteWaterMgt
                   , t.SolidWasteMgt = s.SolidWasteMgt
                   , t.EnviroHarmfulSubstances = s.EnviroHarmfulSubstances
    WHEN NOT MATCHED THEN
        INSERT
        (
            [Member_Id]
           ,[Checklist_Id]
           ,[GreenhouseGasEmissions]
           ,[EnergyEfficiencyConservationAndMgt]
           ,[MgtOfFreshwaterResources]
           ,[EcosystemConservationAndMgt]
           ,[SocialAndCulturalMgt]
           ,[LandUsePlanningAndMgt]
           ,[AirQualityProtection]
           ,[WasteWaterMgt]
           ,[SolidWasteMgt]
           ,[EnviroHarmfulSubstances]
        )
        VALUES
        (   s.[Member_Id]
           ,s.[Checklist_Id]
           ,s.[GreenhouseGasEmissions]
           ,s.[EnergyEfficiencyConservationAndMgt]
           ,s.[MgtOfFreshwaterResources]
           ,s.[EcosystemConservationAndMgt]
           ,s.[SocialAndCulturalMgt]
           ,s.[LandUsePlanningAndMgt]
           ,s.[AirQualityProtection]
           ,s.[WasteWaterMgt]
           ,s.[SolidWasteMgt]
           ,s.[EnviroHarmfulSubstances]);
END;
GO
