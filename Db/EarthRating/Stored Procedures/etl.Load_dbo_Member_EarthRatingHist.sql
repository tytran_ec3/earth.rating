SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==========================================================================================
-- Type			Date		Author	Description
-- Created:		05/09/2017	Ty		Loading EarthRating details to EarthRating.dbo.Member
-- Modified:	26/09/2017	Ty		Updated to a MERGE Statement to allow for updates.
-- ==========================================================================================

CREATE PROCEDURE [etl].[Load_dbo_Member_EarthRatingHist]
AS
BEGIN
    SET NOCOUNT ON;

    MERGE dbo.Member_EarthRatingHist t
    USING etl.Member_EarthRatingHist s
    ON (s.Member_Id = t.Member_Id AND s.Checklist_Episode_Id = t.Checklist_Episode_Id AND s.Checklist_Id = t.Checklist_Id)
    WHEN MATCHED AND (s.EarthRating <> t.EarthRating) THEN
        UPDATE SET t.EarthRating = s.EarthRating
    WHEN NOT MATCHED THEN
        INSERT
        (
            Member_Id
			, Checklist_Episode_Id
            , Checklist_Id
            , ChecklistName
            , EarthRating
        )
        VALUES
        (s.Member_Id, s.Checklist_Episode_Id, s.Checklist_Id, s.ChecklistName, s.EarthRating);
END;
GO
