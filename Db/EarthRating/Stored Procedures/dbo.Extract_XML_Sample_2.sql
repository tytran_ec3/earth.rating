SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Extract_XML_Sample_2]
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @xmlResult XML,
            @strResult NVARCHAR(MAX);

    SET @xmlResult =
    (
        /** START of EarthRating Element **/
        SELECT CONVERT(VARCHAR, GETUTCDATE(), 126) '@DateTimeOfDataUTC',
               (
                   /** START of Members Element**/
                   SELECT mm.Member_GUID '@Id',
                          me.name 'Name',
                          co.name 'Country',
                          me.isactive 'IsActive'
                   FROM EC3Global.dbo.Member me
                       JOIN EC3Global.dbo.MemberContact mc ON mc.member_id = me.member_id
                       JOIN EC3Global.dbo.[Address] ad ON mc.address_id = ad.address_id
                       JOIN EC3Global.dbo.Country co ON co.country_id = ad.country_id
                       JOIN EarthRating.dbo.MemberMapping mm ON me.member_id = mm.Member_ID
                   FOR XML PATH('Member'), ROOT('Members'), TYPE
               /** END of Members Element **/
               ),
               (
                   /** START of MemberEarthRating **/
                   SELECT mm.Member_GUID '@Id',
                          5 'Commitment',
                          5 'Governance',
                          5 'RiskManagement',
                          5 'Communication',
                          5 'Performance',
                          (
                              /** START of KeyPerformanceAreas Element **/
                              SELECT 5 'GreenhouseGasEmissions',
                                     5 'EnergyEfficiencyConservationAndManagement',
                                     5 'ManagementOfFreshwaterResources',
                                     5 'EcosystemConservationAndManagement',
                                     5 'SocialAndCulturalManagement',
                                     5 'LandUsePlanningAndManagement',
                                     5 'AirQualityProtection',
                                     5 'WastewaterManagement',
                                     5 'SolidWasteManagement',
                                     5 'EnvironmentallyHarmfulSubstances'
                              FOR XML PATH('KeyPerformanceAreas'), TYPE
                          /** END of KeyPerformanceAreas Element **/
                          )
                   FROM EC3Global.dbo.Member me
                       JOIN EC3Global.dbo.MemberContact mc ON mc.member_id = me.member_id
                       JOIN EC3Global.dbo.[Address] ad ON mc.address_id = ad.address_id
                       JOIN EC3Global.dbo.Country co ON co.country_id = ad.country_id
                       JOIN EarthRating.dbo.Member_Mapping mm ON me.member_id = mm.Member_ID
                   /** END of MemberEarthRating **/
                   FOR XML PATH('Member'), ROOT('EarthRating'), TYPE
               )
        FOR XML PATH('EarthCheck'), TYPE
    /** END of EarthRating Element **/
    );

    SET @strResult = CAST(@xmlResult AS NVARCHAR(MAX));
    --SELECT @strResult;
    SELECT @xmlResult;

END;
GO
