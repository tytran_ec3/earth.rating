SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Extract_XML_Sample_v1.2]
AS
BEGIN

   -- File generation


    /*
<EarthCheck
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:noNamespaceSchemaLocation="XSD_FILE_PATH">
*/
    SET NOCOUNT ON;
    DECLARE @xmlResult XML,
            @strResult NVARCHAR(MAX);

    SET @xmlResult =
    (
		-- START of EarthRating Element
        SELECT CONVERT(VARCHAR, GETUTCDATE(), 126) '@DateTimeOfDataUTC',
               (
				   
                   /** START of Member Element (With Members as Root) **/
                   SELECT mm.Member_GUID '@Id',
                          me.Member_Name 'Name',
                          dbo.TRIM(me.Country) 'Country',
                          me.isactive 'IsActive',
						  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 /*ELSE EarthRating*/ END EarthRating,
						  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN dbo.TRIM(me.AchievementLevel) ELSE 'Evaluate' END AS 'AchievementLevel', -- add in -- Optional
						  
                          (
							  /** START of MemberEarthRating **/
							  SELECT TOP 100
							  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE vmer.Commitment END AS Commitment, -- Policy
							  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE vmer.Governance END  AS Governance, -- Compliance
							  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE vmer.RiskManagement END AS RiskManagement, -- Sustainability Approach
							  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE vmer.Communication END AS Communication, 
							  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE vmer.Performance END AS Performance ,( 

								  /** START of KeyPerformanceAreas Element **/
								  SELECT CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE ISNULL(GreenhouseGasEmissions,0) END AS 'GreenhouseGasEmissions',
										 CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE ISNULL(EnergyEfficiencyConservationAndMgt,0) END AS 'EnergyEfficiencyConservationAndManagement',
										 --CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE ISNULL(MgtOfFreshwaterResources,0) END AS 'ManagementOfFreshwaterResources',
										 CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE ISNULL(EcosystemConservationAndMgt,0) END AS 'EcosystemConservationAndManagement',
										 CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE ISNULL(SocialAndCulturalMgt,0) END AS 'SocialAndCulturalManagement',
										 CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE ISNULL(LandUsePlanningAndMgt,0) END AS 'LandUsePlanningAndManagement',
										 CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE ISNULL(AirQualityProtection,0) END AS 'AirQualityProtection',
										 CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE ISNULL(WastewaterMgt,0) END AS 'WastewaterManagement',
										 CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE ISNULL(SolidWasteMgt,0) END AS 'SolidWasteManagement',
										 CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE ISNULL(EnvironmentallyHarmfulSubstances,0) END AS 'EnvironmentallyHarmfulSubstances'
								  FROM dbo.Member m
								  LEFT JOIN [dbo].[Member_KeyPerformance_EarthRating] mkp on mkp.Member_Id = m.Member_Id
								  WHERE m.Member_Id = me.Member_Id
								  FOR XML PATH('KeyPerformanceAreas'), TYPE
								  /** END of KeyPerformanceAreas Element **/
                          )
							  FROM dbo.Member m
							  LEFT JOIN dbo.vw_Member_EarthRating vmer on vmer.Member_Id = m.Member_Id
							  WHERE m.Member_Id = me.Member_Id
								FOR XML PATH('EarthRatingData'), TYPE
							  /** END of MemberEarthRating **/
						  )
                   FROM dbo.Member me
                       JOIN dbo.Member_Mapping mm ON me.member_id = mm.Member_ID
					   --WHERE me.Country = 'Jamaica'
					   --me.AchievementLevel = 'EarthCheck Evaluate' -- testing line
                   FOR XML PATH('Member'), ROOT('Members'), TYPE
				   /** END of Members Element **/
               )
        FOR XML PATH('EarthRating'), TYPE
		/** END of EarthRating Element **/
    );

    SET @strResult = CAST(@xmlResult AS NVARCHAR(MAX));
    --SELECT @strResult;
    SELECT @strResult AS RowData;
END;
GO
