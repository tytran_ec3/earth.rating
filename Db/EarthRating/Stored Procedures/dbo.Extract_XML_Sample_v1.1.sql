SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Extract_XML_Sample_v1.1]
AS
BEGIN

   -- File generation


    /*
<EarthCheck
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:noNamespaceSchemaLocation="XSD_FILE_PATH">
*/
    SET NOCOUNT ON;
    DECLARE @xmlResult XML,
            @strResult NVARCHAR(MAX);

    SET @xmlResult =
    (
		-- START of EarthRating Element
        SELECT CONVERT(VARCHAR, GETUTCDATE(), 126) '@DateTimeOfDataUTC',
               (
				   
                   /** START of Member Element (With Members as Root) **/
                   SELECT mm.Member_GUID '@Id',
                          me.Member_Name 'Name',
                          me.Country 'Country',
                          me.isactive 'IsActive',
						  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE EarthRating END EarthRating,
						  me.AchievementLevel 'AchievementLevel', -- add in -- Optional
						  
                          (
							  /** START of MemberEarthRating **/
							  SELECT
							  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE vmer.Commitment END AS Commitment, -- Policy
							  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE vmer.Governance END  AS Governance, -- Compliance
							  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE vmer.RiskManagement END AS RiskManagement, -- Sustainability Approach
							  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE vmer.Communication END AS Communication, 
							  CASE WHEN me.AchievementLevel <> 'EarthCheck Evaluate' THEN 5 ELSE vmer.Performance END AS Performance ,( 

								  /** START of KeyPerformanceAreas Element **/
								  SELECT 5 'GreenhouseGasEmissions',
										 5 'EnergyEfficiencyConservationAndManagement',
										 5 'ManagementOfFreshwaterResources',
										 5 'EcosystemConservationAndManagement',
										 5 'SocialAndCulturalManagement',
										 5 'LandUsePlanningAndManagement',
										 5 'AirQualityProtection',
										 5 'WastewaterManagement',
										 5 'SolidWasteManagement',
										 5 'EnvironmentallyHarmfulSubstances'
								  FOR XML PATH('KeyPerformanceAreas'), TYPE
								  /** END of KeyPerformanceAreas Element **/

                          )
							  FROM dbo.vw_Member_EarthRating vmer
									WHERE vmer.Member_Id = me.Member_Id
									FOR XML PATH('EarthRatingData'), TYPE
							  /** END of MemberEarthRating **/
						  )
                   FROM dbo.Member me
                       JOIN dbo.Member_Mapping mm ON me.member_id = mm.Member_ID
					   WHERE me.AchievementLevel = 'EarthCheck Evaluate' -- testing line
                   FOR XML PATH('Member'), ROOT('Members'), TYPE
				   /** END of Members Element **/
               )
        FOR XML PATH('EarthRating'), TYPE
		/** END of EarthRating Element **/
    );

    SET @strResult = CAST(@xmlResult AS NVARCHAR(MAX));
    --SELECT @strResult;
    SELECT @xmlResult;
END;
GO
