SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ==========================================================================================
-- Type			Date		Author  Description
-- Created:		01/10/2017	TT		Get Member Earth ratings. Used by EarthRating API. /Members & /Member (UpdatedSince Header)
-- Modified:	
-- ==========================================================================================

CREATE PROCEDURE [dbo].[spGetEarthRatingMembers] @FromDate DATETIME = NULL
AS
BEGIN

    SET NOCOUNT ON;

	IF (@FromDate IS NOT NULL)
	BEGIN
		SELECT Member_Guid
					   , Member_Name
					   , Country
					   , IsActive
					   , AchievementLevel
					   , EarthRating
					   , Commitment
					   , Governance
					   , RiskManagement
					   , Performance
					   , Communication
					   , LastUpdated
				FROM dbo.vw_Member_EarthRating
				WHERE LastUpdated > @FromDate
	END
	ELSE
	BEGIN
		SELECT Member_Guid
					   , Member_Name
					   , Country
					   , IsActive
					   , AchievementLevel
					   , EarthRating
					   , Commitment
					   , Governance
					   , RiskManagement
					   , Performance
					   , Communication
					   , LastUpdated
				FROM dbo.vw_Member_EarthRating
				
	END

END

GO
