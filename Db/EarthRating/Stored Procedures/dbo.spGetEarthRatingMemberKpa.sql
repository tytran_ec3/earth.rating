SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==========================================================================================
-- Type			Date		Author  Description
-- Created:		01/10/2017	TT		Get Member Earth ratings Key Performance Areas. Used by EarthRating API.
-- Modified:	16/11/2017  TT		Added Member exist checks and error return
-- ==========================================================================================

CREATE PROCEDURE [dbo].[spGetEarthRatingMemberKpa] @MemberGuid UNIQUEIDENTIFIER
AS
BEGIN

    SET NOCOUNT ON;

	DECLARE @err_message nvarchar(255)
	IF NOT EXISTS(SELECT TOP(1) * FROM dbo.Member_Guid WHERE Member_Guid = @MemberGuid)
		BEGIN
			SET @err_message = 'Member ' + CAST(@MemberGuid AS VARCHAR(40)) + ' not found.'
				RAISERROR (@err_message,17, 1) 
		END	

    SELECT mkp.Member_Guid
           , mkp.[GreenhouseGasEmissions]
           , mkp.[EnergyEfficiencyConservationAndMgt] 'EnergyEfficiencyConservationAndManagement'
           , mkp.[MgtOfFreshwaterResources] 'ManagementOfFreshWaterResources'
           , mkp.[EcosystemConservationAndMgt] 'EcosystemConservationAndManagement'
           , mkp.[SocialAndCulturalMgt] 'SocialAndCulturalManagement'
           , mkp.[LandUsePlanningAndMgt] 'LandUsePlanningAndManagement'
           , mkp.[AirQualityProtection]
           , mkp.[WasteWaterMgt] 'WastewaterManagement'
           , mkp.[SolidWasteMgt] 'SolidWasteManagement'
           , mkp.[EnviroHarmfulSubstances] 'EnvironmentallyHarmfulSubstances'
    FROM [dbo].[vw_Member_EarthRating_Kpa] mkp
    WHERE mkp.Member_Guid = @MemberGuid;

END;
GO
