SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [dbo].[vw_Member_EarthRating_Kpa]
AS
SELECT mg.Member_Guid
       , mkp.[GreenhouseGasEmissions]
       , mkp.[EnergyEfficiencyConservationAndMgt]
       , mkp.[MgtOfFreshwaterResources]
       , mkp.[EcosystemConservationAndMgt]
       , mkp.[SocialAndCulturalMgt]
       , mkp.[LandUsePlanningAndMgt]
       , mkp.[AirQualityProtection]
       , mkp.[WasteWaterMgt]
       , mkp.[SolidWasteMgt]
       , mkp.[EnviroHarmfulSubstances]
FROM [EarthRating].[dbo].[Member_KeyPerformance] mkp
    JOIN dbo.Member m ON m.Member_Id = mkp.Member_Id
	JOIN dbo.Member_Guid mg ON mg.Member_Id = m.Member_Id

	-- Add Certified defaults to 5 EarthRating to view.
	UNION
	SELECT mg.Member_Guid
       , 5 [GreenhouseGasEmissions]
       , 5 [EnergyEfficiencyConservationAndMgt]
       , 5 [MgtOfFreshwaterResources]
       , 5 [EcosystemConservationAndMgt]
       , 5 [SocialAndCulturalMgt]
       , 5 [LandUsePlanningAndMgt]
       , 5 [AirQualityProtection]
       , 5 [WasteWaterMgt]
       , 5 [SolidWasteMgt]
       , 5 [EnviroHarmfulSubstances]
	   FROM dbo.Member m
	   JOIN dbo.Member_Guid mg ON mg.Member_Id = m.Member_Id
	   WHERE AchievementLevel NOT IN ( 'EarthCheck Evaluate', 'EarthCheck', 'EarthCheck Evaluate Plus', 'Benchmarked Bronze' ) -- Add to config table
       AND IsActive = 1;

GO
