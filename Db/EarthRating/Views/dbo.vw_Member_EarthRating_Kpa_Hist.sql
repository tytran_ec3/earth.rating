SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vw_Member_EarthRating_Kpa_Hist]
AS
WITH Eval AS (
SELECT mg.Member_Guid
	   , ROW_NUMBER() OVER(PARTITION BY mg.Member_Guid ORDER BY mkp.Checklist_Episode_Id) -1 [Year] -- Zero out single entries for removal
       , mkp.[GreenhouseGasEmissions]
       , mkp.[EnergyEfficiencyConservationAndMgt]
       , mkp.[MgtOfFreshwaterResources]
       , mkp.[EcosystemConservationAndMgt]
       , mkp.[SocialAndCulturalMgt]
       , mkp.[LandUsePlanningAndMgt]
       , mkp.[AirQualityProtection]
       , mkp.[WasteWaterMgt]
       , mkp.[SolidWasteMgt]
       , mkp.[EnviroHarmfulSubstances]
FROM [dbo].[Member_KeyPerformanceHist] mkp
    JOIN dbo.Member m ON m.Member_Id = mkp.Member_Id
	JOIN dbo.Member_Guid mg ON mg.Member_Id = m.Member_Id
  )
  SELECT Eval.Member_Guid
	   , Eval.Year
       , Eval.GreenhouseGasEmissions
       , Eval.EnergyEfficiencyConservationAndMgt
       , Eval.MgtOfFreshwaterResources
       , Eval.EcosystemConservationAndMgt
       , Eval.SocialAndCulturalMgt
       , Eval.LandUsePlanningAndMgt
       , Eval.AirQualityProtection
       , Eval.WasteWaterMgt
       , Eval.SolidWasteMgt
       , Eval.EnviroHarmfulSubstances
        FROM Eval
		WHERE Eval.[Year] <> 0 -- Removing single entries
	-- Add Certified defaults to 5 EarthRating to view.
	-- TODO: Only show history for year in Certified Program.
	UNION
	SELECT mg.Member_Guid
	   , 0 [Year]
       , 5 [GreenhouseGasEmissions]
       , 5 [EnergyEfficiencyConservationAndMgt]
       , 5 [MgtOfFreshwaterResources]
       , 5 [EcosystemConservationAndMgt]
       , 5 [SocialAndCulturalMgt]
       , 5 [LandUsePlanningAndMgt]
       , 5 [AirQualityProtection]
       , 5 [WasteWaterMgt]
       , 5 [SolidWasteMgt]
       , 5 [EnviroHarmfulSubstances]
	   FROM dbo.Member m
	   JOIN dbo.Member_Guid mg ON mg.Member_Id = m.Member_Id
	   WHERE AchievementLevel NOT IN ( 'EarthCheck Evaluate', 'EarthCheck', 'EarthCheck Evaluate Plus', 'Benchmarked Bronze' ) -- Add to config table
       AND IsActive = 1;

GO
