SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[vw_Member_EarthRating_Hist]
AS
WITH cte
AS (
   SELECT pvt.Member_Guid
          , pvt.Member_Name
          , pvt.Country
          , pvt.IsActive
		  , ROW_NUMBER() OVER(PARTITION BY Member_Guid ORDER BY Checklist_Episode_Id)-1 [YearBy] -- Zero out single entries for removal
          , pvt.AchievementLevel
          , [Policy]
          , [Benchmarking]
          , [Compliance]
          , [Sustainability Approach]
          , [Performance]
          , [Communication]
          , [Interpretation]
          , pvt.LastUpdated
   FROM
   (
       SELECT DISTINCT
           mg.Member_Guid
           , m.Member_Name
           , m.Country
           , m.IsActive
		   , mer.Checklist_Episode_Id
           , m.AchievementLevel
           , mer.ChecklistName
           , mer.EarthRating
           , CASE
                 WHEN m.[WH_DateModified] IS NULL THEN
                     m.[WH_DateCreated]
                 ELSE
                     m.[WH_DateModified]
             END 'LastUpdated'
       FROM dbo.Member_EarthRatingHist AS mer
           JOIN dbo.Member m ON m.Member_Id = mer.Member_Id
		   JOIN dbo.Member_Guid mg ON mg.Member_Id = m.Member_Id
   ) AS src
   PIVOT
   (
       AVG(EarthRating)
       FOR ChecklistName IN ([Policy], [Benchmarking], [Compliance], [Sustainability Approach], [Performance]
                             , [Communication], [Interpretation]
                            )
   ) AS pvt
   )
SELECT cte.Member_Guid
       , cte.Member_Name
       , cte.Country
       , cte.IsActive
	   ,[YearBy]
       , cte.AchievementLevel
       , (
             SELECT (ISNULL(cte.Policy, 0) + ISNULL(cte.Benchmarking, 0) + ISNULL(cte.Compliance, 0)
                     + ISNULL(cte.[Sustainability Approach], 0) + ISNULL(cte.Performance, 0)
                     + ISNULL(cte.Communication, 0) + ISNULL(cte.Interpretation, 0)
                    )
                    / CASE WHEN cte.Policy IS NULL THEN 1 ELSE 1 
						+ CASE WHEN cte.Benchmarking IS NULL THEN 1 ELSE 1
							+ CASE WHEN cte.Compliance IS NULL THEN 1 ELSE 1
								+ CASE WHEN cte.[Sustainability Approach] IS NULL THEN 1 ELSE 1
									+ CASE WHEN cte.Performance IS NULL THEN 1 ELSE 1
										+ CASE WHEN cte.Communication IS NULL THEN 1 ELSE 1
											+ CASE WHEN cte.Interpretation IS NULL THEN 1 ELSE 1 END
                                          END
                                      END
                                  END
                              END
                          END
                      END
         ) AS [EarthRating]
       , ISNULL(cte.Policy, 1) AS [Commitment]
       , ISNULL(cte.Benchmarking, 1) AS [Benchmarking]
       , ISNULL(cte.Compliance, 1) AS [Governance]
       , ISNULL(cte.[Sustainability Approach], 1) AS [RiskManagement]
       , ISNULL(cte.Performance, 1) AS [Performance]
       , ISNULL(cte.Communication, 1) AS [Communication]
       , ISNULL(cte.Interpretation, 1) AS [Interpretation]
       , cte.LastUpdated
FROM cte
WHERE cte.IsActive = 1 AND cte.YearBy > 0

-- Add Certified defaults to 5 EarthRating to view.
UNION
SELECT mg.[Member_Guid]
       , [Member_Name]
       , [Country]
       , [IsActive]
	   , NULL [YearBy]
       , [AchievementLevel]
       , 5 [EarthRating]
       , 5 [Commitment]
       , 5 [Benchmarking]
       , 5 [Governance]
       , 5 [RiskManagement]
       , 5 [Performance]
       , 5 [Communication]
       , 5 [Interpretation]
       , WH_DateModified [LastUpdated]
FROM dbo.Member m
JOIN dbo.Member_Guid mg ON mg.Member_Id = m.Member_Id
WHERE AchievementLevel NOT IN ( 'EarthCheck Evaluate', 'EarthCheck', 'EarthCheck Evaluate Plus', 'Benchmarked Bronze' ) -- Add to config table
      AND IsActive = 1;

GO
