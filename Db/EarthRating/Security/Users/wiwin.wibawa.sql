IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'wiwin.wibawa')
CREATE LOGIN [wiwin.wibawa] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [wiwin.wibawa] FOR LOGIN [wiwin.wibawa]
GO
