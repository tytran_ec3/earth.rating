IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'earthrating_web_user_test')
CREATE LOGIN [earthrating_web_user_test] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [earthrating_web_user_test] FOR LOGIN [earthrating_web_user_test]
GO
