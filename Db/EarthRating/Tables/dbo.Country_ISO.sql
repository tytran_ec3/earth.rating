CREATE TABLE [dbo].[Country_ISO]
(
[Code] [nvarchar] (128) COLLATE Latin1_General_CI_AS NULL,
[Name] [nvarchar] (128) COLLATE Latin1_General_CI_AS NULL,
[Year] [nvarchar] (128) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
