CREATE TABLE [dbo].[Member_EarthRatingHist]
(
[Member_Id] [int] NOT NULL,
[Checklist_Episode_Id] [int] NOT NULL,
[Checklist_Id] [int] NOT NULL,
[ChecklistName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[EarthRating] [tinyint] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_DateCreated] [datetime] NULL CONSTRAINT [DF__Member_Ea__WH_Da__69C6B1F5] DEFAULT (getdate()),
[WH_DateModified] [datetime] NOT NULL CONSTRAINT [DF__Member_Ea__WH_Da__6ABAD62E] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Member_EarthRatingHist] ADD CONSTRAINT [PK_Member_EarthRatingHist] PRIMARY KEY CLUSTERED  ([Member_Id], [Checklist_Episode_Id], [Checklist_Id]) ON [PRIMARY]
GO
