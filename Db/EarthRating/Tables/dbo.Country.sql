CREATE TABLE [dbo].[Country]
(
[Country_Id] [int] NOT NULL,
[Name] [nvarchar] (80) COLLATE Latin1_General_CI_AS NOT NULL,
[NameIso] [nvarchar] (80) COLLATE Latin1_General_CI_AS NULL,
[Code] [char] (3) COLLATE Latin1_General_CI_AS NULL,
[CodeIso] [nvarchar] (80) COLLATE Latin1_General_CI_AS NULL,
[EffectiveDate] [date] NULL,
[EndDate] [date] NULL,
[EffectiveStatus] [char] (1) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_dbo_Country_Delete] ON [dbo].[Country] FOR DELETE AS INSERT INTO [audit_dbo].[Country] (Country_Id,Name,NameIso,Code,CodeIso,EffectiveDate,EndDate,EffectiveStatus,AuditAction) SELECT Country_Id,Name,NameIso,Code,CodeIso,EffectiveDate,EndDate,EffectiveStatus,'D' FROM Deleted
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_dbo_Country_Insert] ON [dbo].[Country] FOR INSERT AS INSERT INTO [audit_dbo].[Country] (Country_Id,Name,NameIso,Code,CodeIso,EffectiveDate,EndDate,EffectiveStatus,AuditAction) SELECT Country_Id,Name,NameIso,Code,CodeIso,EffectiveDate,EndDate,EffectiveStatus,'I' FROM Inserted
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_dbo_Country_Update] ON [dbo].[Country] FOR UPDATE AS INSERT INTO [audit_dbo].[Country] (Country_Id,Name,NameIso,Code,CodeIso,EffectiveDate,EndDate,EffectiveStatus,AuditAction) SELECT Country_Id,Name,NameIso,Code,CodeIso,EffectiveDate,EndDate,EffectiveStatus,'U' FROM Inserted
GO
ALTER TABLE [dbo].[Country] ADD CONSTRAINT [CK_Country_EffectiveStatus] CHECK (([EffectiveStatus]='W' OR [EffectiveStatus]='I' OR [EffectiveStatus]='A'))
GO
ALTER TABLE [dbo].[Country] ADD CONSTRAINT [PK_dbo_Country] PRIMARY KEY CLUSTERED  ([Country_Id]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
