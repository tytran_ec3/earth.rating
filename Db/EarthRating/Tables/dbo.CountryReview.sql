CREATE TABLE [dbo].[CountryReview]
(
[ISO] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F2] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F3] [float] NULL,
[EC3 Admin] [float] NULL,
[F5] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F6] [float] NULL,
[F7] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F8] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F9] [float] NULL,
[F10] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F11] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F12] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F13] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F14] [float] NULL,
[F15] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F16] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F17] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F18] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F19] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F20] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F21] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F22] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F23] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[F24] [float] NULL
) ON [PRIMARY]
GO
