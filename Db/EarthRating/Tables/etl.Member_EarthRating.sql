CREATE TABLE [etl].[Member_EarthRating]
(
[Member_Id] [int] NOT NULL,
[Checklist_Id] [int] NULL,
[ChecklistName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[EarthRating] [tinyint] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
