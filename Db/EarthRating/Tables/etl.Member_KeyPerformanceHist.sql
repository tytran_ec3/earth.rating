CREATE TABLE [etl].[Member_KeyPerformanceHist]
(
[Member_Id] [int] NOT NULL,
[Checklist_Episode_Id] [int] NULL,
[Checklist_Id] [int] NULL,
[GreenhouseGasEmissions] [int] NULL,
[EnergyEfficiencyConservationAndMgt] [int] NULL,
[MgtOfFreshwaterResources] [int] NULL,
[EcosystemConservationAndMgt] [int] NULL,
[SocialAndCulturalMgt] [int] NULL,
[LandUsePlanningAndMgt] [int] NULL,
[AirQualityProtection] [int] NULL,
[WastewaterMgt] [int] NULL,
[SolidWasteMgt] [int] NULL,
[EnviroHarmfulSubstances] [int] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_RunGroup] [int] NULL
) ON [PRIMARY]
GO
