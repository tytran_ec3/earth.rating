CREATE TABLE [dbo].[Country_EC3]
(
[country_id] [int] NOT NULL,
[name] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[code] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
