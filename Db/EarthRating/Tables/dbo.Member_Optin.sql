CREATE TABLE [dbo].[Member_Optin]
(
[Member_Id] [int] NULL,
[Optin_Status] [bit] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Member_Optin_WH_DateCreated] DEFAULT (getdate()),
[WH_DateModified] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[TR_Member_Optin_Update]
ON [dbo].[Member_Optin]
AFTER UPDATE
AS
BEGIN

SET NOCOUNT ON;
UPDATE dbo.Member_Optin
SET WH_DateModified = GETDATE()
FROM inserted i
WHERE dbo.Member_Optin.WH_Id = i.WH_Id
END
GO
