CREATE TABLE [audit_dbo].[Country]
(
[AuditId] [bigint] NOT NULL IDENTITY(1, 1),
[Country_Id] [int] NOT NULL,
[Name] [nvarchar] (160) COLLATE Latin1_General_CI_AS NOT NULL,
[NameIso] [nvarchar] (160) COLLATE Latin1_General_CI_AS NULL,
[Code] [char] (3) COLLATE Latin1_General_CI_AS NULL,
[CodeIso] [nvarchar] (160) COLLATE Latin1_General_CI_AS NULL,
[EffectiveDate] [date] NULL,
[EndDate] [date] NULL,
[EffectiveStatus] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[AuditAction] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AuditDate] [datetime] NOT NULL CONSTRAINT [DF_dbo_Country_AuditDate] DEFAULT (getdate()),
[AuditUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_dbo_Country_AuditUser] DEFAULT (suser_sname()),
[AuditApp] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_dbo_Country_AuditApp] DEFAULT (('App=('+rtrim(isnull(app_name(),'')))+') ')
) ON [PRIMARY]
GO
ALTER TABLE [audit_dbo].[Country] ADD CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED  ([AuditId]) ON [PRIMARY]
GO
