CREATE TABLE [etl].[Member]
(
[Member_Id] [int] NOT NULL,
[Member_Name] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Country] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[IsActive] [bit] NOT NULL,
[Product] [nvarchar] (80) COLLATE Latin1_General_CI_AS NULL,
[AchievementLevel] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
