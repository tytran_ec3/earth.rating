CREATE TABLE [dbo].[Member_Guid]
(
[Member_Id] [int] NOT NULL,
[Member_Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF__Member_Gu__Membe__2B5F6B28] DEFAULT (newid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Member_Guid] ADD CONSTRAINT [PK_dbo_Member_Guid] PRIMARY KEY CLUSTERED  ([Member_Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Member_Guid] ADD CONSTRAINT [FK_Member_Guid_Member] FOREIGN KEY ([Member_Id]) REFERENCES [dbo].[Member] ([Member_Id])
GO
