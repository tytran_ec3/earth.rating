CREATE TABLE [dbo].[Member]
(
[Member_Id] [int] NOT NULL,
[Member_Name] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Country] [nvarchar] (80) COLLATE Latin1_General_CI_AS NULL,
[IsActive] [bit] NOT NULL,
[Product] [nvarchar] (80) COLLATE Latin1_General_CI_AS NULL,
[AchievementLevel] [nvarchar] (200) COLLATE Latin1_General_CI_AS NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Member_WH_DateCreated] DEFAULT (getdate()),
[WH_DateModified] [datetime] NOT NULL CONSTRAINT [DF__Member__WH_DateM__48EFCE0F] DEFAULT (getdate())
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		TT
-- Create date: 2017-09-25
-- Description:	Used to populate Member GUID column
-- =============================================
CREATE TRIGGER [dbo].[TR_Member_Insert]
   ON  [dbo].[Member]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	INSERT INTO Member_Guid (Member_Id)
	SELECT i.Member_Id 
	FROM Inserted i
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		TT
-- Create date: 2017-09-25
-- Description:	Used to populate WH_DateModified column
-- =============================================
CREATE TRIGGER [dbo].[TR_Member_Update]
   ON  [dbo].[Member]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

    UPDATE dbo.Member
	SET WH_DateModified =  GETDATE() 
	FROM Inserted i
	JOIN dbo.Member m ON m.WH_Id = i.WH_Id
END
GO
ALTER TABLE [dbo].[Member] ADD CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED  ([Member_Id]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Member Details with EarthRating', 'SCHEMA', N'dbo', 'TABLE', N'Member', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Program Achievement Level', 'SCHEMA', N'dbo', 'TABLE', N'Member', 'COLUMN', N'AchievementLevel'
GO
EXEC sp_addextendedproperty N'MS_Description', N'CRM Member Country', 'SCHEMA', N'dbo', 'TABLE', N'Member', 'COLUMN', N'Country'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Member Active Status (Based on CRM Member status and Product Status)', 'SCHEMA', N'dbo', 'TABLE', N'Member', 'COLUMN', N'IsActive'
GO
EXEC sp_addextendedproperty N'MS_Description', N'CRM Member ID', 'SCHEMA', N'dbo', 'TABLE', N'Member', 'COLUMN', N'Member_Id'
GO
EXEC sp_addextendedproperty N'MS_Description', N'CRM Member Name', 'SCHEMA', N'dbo', 'TABLE', N'Member', 'COLUMN', N'Member_Name'
GO
