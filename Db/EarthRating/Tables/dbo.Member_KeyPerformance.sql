CREATE TABLE [dbo].[Member_KeyPerformance]
(
[Member_Id] [int] NOT NULL,
[Checklist_Id] [int] NULL,
[GreenhouseGasEmissions] [int] NULL,
[EnergyEfficiencyConservationAndMgt] [int] NULL,
[MgtOfFreshwaterResources] [int] NULL,
[EcosystemConservationAndMgt] [int] NULL,
[SocialAndCulturalMgt] [int] NULL,
[LandUsePlanningAndMgt] [int] NULL,
[AirQualityProtection] [int] NULL,
[WasteWaterMgt] [int] NULL,
[SolidWasteMgt] [int] NULL,
[EnviroHarmfulSubstances] [int] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_DateCreated] [datetime] NOT NULL CONSTRAINT [DF__Member_Ke__WH_Da__2DB1C7EE] DEFAULT (getdate()),
[WH_DateModified] [datetime] NOT NULL CONSTRAINT [DF__Member_Ke__WH_Da__2CBDA3B5] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Member_KeyPerformance] ADD CONSTRAINT [PK_dbo_Member_KeyPerformance] PRIMARY KEY CLUSTERED  ([Member_Id]) ON [PRIMARY]
GO
