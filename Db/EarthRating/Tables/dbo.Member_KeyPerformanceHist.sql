CREATE TABLE [dbo].[Member_KeyPerformanceHist]
(
[Member_Id] [int] NOT NULL,
[Checklist_Episode_Id] [int] NOT NULL,
[Checklist_Id] [int] NOT NULL,
[GreenhouseGasEmissions] [int] NULL,
[EnergyEfficiencyConservationAndMgt] [int] NULL,
[MgtOfFreshwaterResources] [int] NULL,
[EcosystemConservationAndMgt] [int] NULL,
[SocialAndCulturalMgt] [int] NULL,
[LandUsePlanningAndMgt] [int] NULL,
[AirQualityProtection] [int] NULL,
[WasteWaterMgt] [int] NULL,
[SolidWasteMgt] [int] NULL,
[EnviroHarmfulSubstances] [int] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_DateCreated] [datetime] NOT NULL CONSTRAINT [DF__Member_Ke__WH_Da__6BAEFA67] DEFAULT (getdate()),
[WH_DateModified] [datetime] NOT NULL CONSTRAINT [DF__Member_Ke__WH_Da__6CA31EA0] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Member_KeyPerformanceHist] ADD CONSTRAINT [PK_dbo_Member_KeyPerformanceHist] PRIMARY KEY CLUSTERED  ([Member_Id], [Checklist_Episode_Id], [Checklist_Id]) ON [PRIMARY]
GO
