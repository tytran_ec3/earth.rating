CREATE TABLE [dbo].[Member_EarthRating]
(
[Member_Id] [int] NOT NULL,
[Checklist_Id] [int] NOT NULL,
[ChecklistName] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL,
[EarthRating] [tinyint] NULL,
[WH_Id] [int] NOT NULL IDENTITY(1, 1),
[WH_DateCreated] [datetime] NULL CONSTRAINT [DF__Member_Ea__WH_Da__382F5661] DEFAULT (getdate()),
[WH_DateModified] [datetime] NOT NULL CONSTRAINT [DF__Member_Ea__WH_Da__39237A9A] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Member_EarthRating] ADD CONSTRAINT [PK_Member_EarthRating] PRIMARY KEY CLUSTERED  ([Member_Id], [Checklist_Id]) ON [PRIMARY]
GO
